package com.yedam.loop;

import java.util.Scanner;

public class Exam02 {

	public static void main(String[] args) {
		int i = 1;
		int sum = 0;
		while (i <= 5) {
			sum = sum + i;
			System.out.println(sum);
			i++;

		}
		i = 0;
		while (i <= 100) {
			if (i % 2 == 0) {
				System.out.println(i);
			}
			i++;
		}

//		boolean flag = true;
//		i=0;
//		while(flag) {
//			if(i==50) {
//				//break;  무한 루프 탈출
//				flag = false;
//			}
//			i++;
//		}
		System.out.println("end of prog");

		// 계산기 프로그램~
		boolean flag = true;
		Scanner sc = new Scanner(System.in);

		// while문 이용 무한루프 생성 -> false값으로 탈출
		while (flag) {
			System.out.println("1. 더하기 | 2. 빼기 | 3. 곱하기 | 4. 종료");
			System.out.println("입력>");
			int no = Integer.parseInt(sc.nextLine());

			switch (no) {
			case 1:
				System.out.println("더하고 하는 두 수를 입력하세요.");
				System.out.println("1>");
				int num = Integer.parseInt(sc.nextLine());
				System.out.println("2>");
				int num2 = Integer.parseInt(sc.nextLine());
				System.out.println(num + "," + num2 + "의 결과 : " + (num + num2));
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				System.out.println("프로그램을 종료합니다.");
				flag = false;
				break;
			default:
				System.out.println("번호를 잘못 입력하셨습니다.");
				break;
			}

		}

		// 게임 만들기
		// 가위, 바위, 보
		// 앞, 뒤 맞추기
		flag = true;
		int money;
		while (flag) {
			System.out.println("====insert Coin====");
			money = Integer.parseInt(sc.nextLine());
			while (money / 500 > 0) {
				// 한 판에 500원
				System.out.println((money / 500) + "번의 기회가 있습니다.");

				System.out.println("1. 가위바위보 | 2. 앞 뒤 맞추기 | 3. 종료");
				// 숫자 야구 게임(4개의 무작위 번호 맞추는것),
				int gameNo = Integer.parseInt(sc.nextLine());

				// 가위(1) 바위(2) 보(3)
				if (gameNo == 1) {
					System.out.println("가위, 바위, 보 중에서 하나를 입력하세요");
					// 사용자
					String RSP = sc.nextLine();
					// 컴퓨터
					int randomNo = (int) (Math.random() * 3) + 1; // 1~3사이의 정수

					if (RSP.equals("가위")) {
						if (randomNo == 1) {
							System.out.println("비겼다.");
						} else if (randomNo == 2) {
							System.out.println("졌다.");
						} else {
							System.out.println("이겼다.");
						}
					} else if (RSP.equals("바위")) {
						if (randomNo == 1) {
							System.out.println("이겼다.");
						} else if (randomNo == 2) {
							System.out.println("비겼다.");
						} else {
							System.out.println("졌다.");
						}
					} else if (RSP.equals("보")) {
						if (randomNo == 1) {
							System.out.println("졌다.");
						} else if (randomNo == 2) {
							System.out.println("이겼다.");
						} else {
							System.out.println("비겼다.");
						}

					} // 앞 뒤 맞추기
					else if (gameNo == 0) {
						System.out.println("앞과 뒤중 하나를 선택하시오");
						String FB = sc.nextLine();
						int randomNo2 = (int) (Math.random() * 1) + 1;

						if (FB.equals("앞")) {
							if (randomNo2 == 0) {
								System.out.println("정답입니다.");
							} else {
								System.out.println("틀렸습니다.");
							}
						} else if (FB.equals("뒤")) {
							if (randomNo2 == 1) {
								System.out.println("정답입니다.");
							} else {
								System.out.println("틀렸습니다.");
							}
						}
						// }
						// 종료
						else if (gameNo == 3) {
							System.out.println("Bye");
							break;
						}
					}

				}
			}
		}}}
