package com.yedam.oop;		// 4주차 22.11.24

// SmartPhone이라는 설계도
public class SmartPhone {
	// 필드
	// 객체의 정보를 저장
	String name;
	String maker;
	int price;
	 
	//생성자(클래스 이름과 똑같이 부여해서 만듦)
	// 자바에서 생성자가 클래스 내부에 "하나도 없을 때 " 기본 생성자를
	// 알아서 만들고 객체를 생성.
	public SmartPhone() {		// 기본 생성자
		this.name = "iphone14pRO";
	}
	// 생성자 오버로딩 (매개변수를 달리하는 생성자 여러개 선언)
	public SmartPhone(String name) {
		// 객체를 만들 때 내가 원하는 행동 또는 데이터 저장 등등
		// 할때 여기에 내용을 구현
	}
	public SmartPhone(int price) {
		
	}
	public SmartPhone(String name, int price) {
		this.name = name;				// this. -> 페이지에 있는 필드값을 가져옴
		this.price = price;
	}
	
	// 외부에서 가져온 매개변수데이터를 필드의 데이터에 넣으려고 함
	// 필드의 데이터를 초기화를 시키면서 생성자를 만들 때
	// 매개변수에 넣은 데이터를 명확하게 넣기 위해
	// 클래스 내부에 존재하는 필드를 가져올 때 this 사용!   // this -> 내 자신, 클래스
	
	public SmartPhone(String name, String maker, int price) {
//		this.name = name;
//		this.maker = maker;
//		this.price = price;
	}
	// 물건을 만들때 정보를 안넣는다 / 정보를 넣는다 
	// 생성자의 역할 - 초기화 담당 - 필드의 정보를 초기화 할 수 있냐없냐 여부
		
	
	
	// 메소드
	// 객체의 기능을 정의
	void call() {
		System.out.println(name + "전화를 겁니다.");
	}
	void hangUp() {
		System.out.println(name + "전화를 끊습니다.");
	}
	
	//1) 리턴 타입이 없는 경우 : void
	void getInfo(int no) {
		System.out.println("매개 변수 : " + no);
	}
	
	//2) 리턴 타입이 있는 경우 
	//1. 기본 타입 : int, double, long,...
	//2. 참초 타입 : String, 배열, 클래스,...
	//2-1) 리턴 타입이 기본 타입일 경우
	int getInfo(String temp) {
		return 0;
	}
	
	//2-2) 리턴 타입이 참조 타입일 경우
	String[] getInfo(String[] temp) {
		
		return temp;			// return 타입뒤에 똑같은 데이터 타입을 맞춰줘야함.
	}
	
	
	
	
	
	
	
	
	




}	
