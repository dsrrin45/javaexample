package com.yedam.oop;

public class SelfStudy {
	public static void main(String[] args) {
		//필드
				Book SelfStudy = new Book();
				SelfStudy.name = "혼자 공부하는 자바";
				SelfStudy.content = "";
				SelfStudy.type = "학습서";
				SelfStudy.price = "24000원";
				SelfStudy.publisher = "한빛미디어";
				SelfStudy.isbn= "yedam-001";

				Book linux = new  Book();
				linux.name = "이것이 리눅스다";
				linux.content = "";
				linux.type = "학습서";
				linux.price = "32000원";
				linux.publisher = "한빛미디어";
				linux.isbn = "yedam-002";
				
				Book script = new Book();
				script.name = "자바스크립트 파워북";
				script.content = "";
				script.type = "학습서";
				script.price = "22000원";
				script.publisher = "어포스트";
				script.isbn = "yedam-003";
		
		// 클래스의 정보를 객체들이 다 저장되어있어서 객체를 바탕으로 메소드 호출.		
		SelfStudy.getInfo();
		linux.getInfo();
		script.getInfo();
				
				
				
				
				
		
	}
	
	
	
	
	
}
