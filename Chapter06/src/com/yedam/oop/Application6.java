package com.yedam.oop;

import java.util.Scanner;				// 클래스를 여러개 사용했을 때 어떻게 사용할 것인가.

public class Application6 {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int stdCount = 0;	// 객체의 정보를 받아오기 위해
		Student[] stdAry = null; // 객체가 들어갈 수 있는 배열 생성
		
		
		while(true) {
			System.out.println("1. 학생 수|2.학생 정보 입력 | 3. 학생의 총점, 평균 | 4. 종료");
			System.out.println("입력>");
			int selectNo = Integer.parseInt(sc.nextLine());		// 사용자 입력값
			
			//1. 학생 수
			if(selectNo == 1) {
				System.out.println("학생 수 입력>");
				stdCount = Integer.parseInt(sc.nextLine());

			} else if(selectNo == 2) {
				stdAry = new Student[stdCount];
				for(int i=0; i<stdAry.length; i++) {	// 학생들의 성적을 배열의 크기만큼 반복을 하면서 넣어줌
					Student std = new Student();	// 정보를 넣어줄 빈 객체 생성
					
					// 빈 객체에 다가 정보를 담아주는 작업
					System.out.println("학생 이름>");
					String name  = sc.nextLine();
					std.name = name;		// 필드에 접근하여 정보 받아옴.(넣어줌)
					
					System.out.println("국어 성적>");
					int kor = Integer.parseInt(sc.nextLine());
					std.kor = kor;
					
					System.out.println("수학 성적>");
					std.math = Integer.parseInt(sc.nextLine());
					
					System.out.println("영어 성적>");
					std.eng = Integer.parseInt(sc.nextLine());
					// std안에는 원하는 정보가 다 들어가 있는 상태.
					
					stdAry[i] = std;	// 정보가 들어있는 객체(사람)를 배열(방)에 다가 넣어줌
					// 빈객체 하나 만들고 -> 데이터 넣어주고 (for문으로 반복)
					// -> 배열에 다가 하나씩 넣어줌
					
				}
				
				// 배열에 있는 데이터를 하나 씩 끄집어내서 stdAry[i] 총점과 평균을 구해줌
			} else if(selectNo == 3) {
				for(int i=0; i<stdAry.length; i++) {
					System.out.println(stdAry[i].stdName+" 학생 성적");
					System.out.println("총점: "+ stdAry[i].sum());
					System.out.println("평균: "+ stdAry[i].avg());
					}
				} else if(selectNo == 4) {
					System.out.println("종료");
					break;
				}
			}
			
	}
}
