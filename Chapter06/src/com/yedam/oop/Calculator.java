package com.yedam.oop;

public class Calculator {
	
	//필드
	//정적 필드
	static double pi = 3.14;
	
	
	//생성자
	
	
	
	//메소드				외부에서 호출시 .(닷)연산자를 사용하여 생성자 인식 선택
	//정적 메소드
	static int plus(int x, int y) {
		return x+y;
	}
	
	int sum(int a, int b) {
		result("메소드 연습"); // 같은 공간 인식 가능
		return a+b;
	}
	// 매개변수의 데이터 타입 차이에 따른 오버로딩
	// int -> double
	double sum (double a, double b) {
		return a+b;
	}
	// 매개변수의 갯수에 따른 오버로딩
	// 매개변수가 2-> 1
	int sum(int a) {
		return a;
	}
	// 매개변수의 데이터 타입 차이에 따른 오버로딩
	// int , int -> double, int
	double sum(double a, int b) {
		return a+b;
	}
	// 매개변수의 순서 차이에 따른 오버로딩
	// double, int -> int, double
	double sum(int b, double a) {
		return a+b;
	}
	
	double sub(int a, int b) {  // 리턴의 결과값이 기본타입 double로 자동타입변환됨. // 기본타입과 매개변수의 타입이 달라도 되나봄..
		return a-b;				// 단 다른 곳에 있는 메서드를 호출할때는 해당 메서드의 매개변수 규칙(타입)을 지켜야함.
	}
	
	// return값 가져와서 메인문에서 실행
//	String result(String value) {
//		String temp = "value return 테스트 : " + value;
//		return temp;
//	}
	
	// return값 없이 메소드사용
	void result(String value) {
		System.out.println("value return 테스트 : " + value);
	}
	
	
	
	
}
