package com.yedam.oop;				//4주차 22.11.25

import com.yedam.access.Access;			// < 접근 제한자 실습 >

public class Application07 {
	public static void main(String[] args) {
		Access access = new Access();
	
		//public
		access.free = "free";
		
		//protected
		access.parent = "parent";
				
		//private
		access.privacy = "privacy";
		
		//default
		access.basic = "basic";
	}
}
