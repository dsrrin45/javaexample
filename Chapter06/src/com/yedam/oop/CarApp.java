package com.yedam.oop;

import java.util.Scanner;

import com.yedam.access.Access;	// 패키지명.사용할 클래스 명 <1.다른 패키지의 클래스 사용하기>

public class CarApp {
	Access ac = new Access(); // <2. 오류나는거 찾아서 클릭>
	Scanner sc = new Scanner(System.in); // <3.스캐너 등록 ctrl + shift + o>
	
	static int speed;
	
	void run() {
		System.out.println(speed + "으로 달립니다.");
	}
	// 메소드 영역에 등록된 친구들.
	public static void main(String[] args) {
		//static int speed2 = speed;	//static이 아니기 때문에 speed 오류.
		
		//run();
		CarApp app = new CarApp();	// 내자신 클래스를 객체화한 다음에 인스턴스 필드와 메소드에 접근
		app.run();
		
		
		Car myCar = new Car("포르쉐");		// class에서 복사한 내용을 기반으로 실행하는 거지
											// 가져와서 실행하는건 아님.
		Car yourCar = new Car("벤츠");
		
		//myCar.run();
		//yourCar.run();
		
		// 정적 필드, 메소드를 부르는 방법
		// 정적 멤버가 속해 있는 클래스명.필드 또는 메소드 명
		// 1) 정적 필드 가져오는 방법
		double piRatio = Calculator.pi;
		System.out.println(piRatio);
		// 1) 정적 메소드 가져오는 방법
		int result = Calculator.plus(5, 6);
		System.out.println(result);
		
		// 1) ☆★☆★☆★☆★☆★ 모든 클래스에서 가져와서 사용할 수 있다 -> 공유의 개념
		// 2) ☆★☆★☆★☆★☆★ 너무 남용해서 사용하면 메모리 누수(부족) 현상이 발생 할 수 있다.
		// 3) ☆★주의 할 점 ★☆
		// 메소드 영역에 저장이 됩니다.
		// static 영역 안에서 사용가능
		// - 정적 메소드에서 외부에 정의한 필드, 메소드를 사용하려고 한다면,
		// static이 붙은 필드 또는 메소드만 사용 가능.
		// static 붙이지 않고 사용하고 싶다면 , 
		// 해당 필드와 메소드가 속해 있는 클래스를 인스턴스화 하여서
		// 인스턴스 필드와 인스턴스 메소드를 dot(.)연산자를 통해 가져와서 사용.
		
		
		//Person
		
		Person p1 = new Person("123123-123456", "김또치");
		//final -> nation, ssn
		System.out.println(p1.nation);
		System.out.println(p1.ssn);
		System.out.println(p1.name);
		
		//p1.nation = "USA"; -> final 변경불가 오류
		
		ConstantNo.EARTH_ROUND;
		//원 넓이
		System.out.println(5*5*ConstantNo.PI); //해당 클래스명.필드명
	}
}
