package com.yedam.oop;			// 4주차 22.11.24

public class Application {
	public static void main(String[] args) {
		// SmartPhone 클래스(설계도)를 토대로 iphone14Pro							// 생성자를 통해 클래스의 객체를 초기화 하면서 hip 영역에 새로운 객체 생성
		SmartPhone iphone14Pro = new SmartPhone("Apple", "iphone14Pro", 500);  //생성자에 대응되는 매개변수 대입   // 물건을 생성하면서 데이터를 넣는 방법1
		// 물체의 정보저장(클래스의 필드에 데이터 저장)
		// 클래스의 접근은 .(닷넷)으로 함.
		iphone14Pro.maker = "Apple";			// 객체 만듦과 동시에 데이터를 넣는 방법2
		iphone14Pro.name = "iphone14Pro";
		iphone14Pro.price = 100000;
		// 필드 재사용(덮어쓰기) 가능
		iphone14Pro.price = 500;
		
		// 정보를 기반으로 물체의 기능 구현 
		iphone14Pro.call();
		iphone14Pro.hangUp();
		
		// 필드 정보 읽기
		System.out.println(iphone14Pro.maker);
		System.out.println(iphone14Pro.name);
		System.out.println(iphone14Pro.price);
		
		// SmartPhone 클래스(설계도)기반 객체생성 - *클래스 재사용 가능*
		// 생성한 객체의 주소값으로 클래스(설계도)의 내용이 복사됨 
		// 설계도(클래스)에 정보가 담기는게 아니라 물건(객체)에 정보가 저장됨.
		
//		SmartPhone zfilp4 = new SmartPhone();
//		zfilp4.maker = "samsung";
//		zfilp4.name = "zfilp4";
//		zfilp4.price = 10000;
//		
//		zfilp4.call();
//		zfilp4.hangUp();
//		
//		메소드 선언
		SmartPhone sony = new SmartPhone();
		// 리턴 타입이 없는 메소드
		sony.getInfo(0);
		// 리턴 타입이 int인 메소드
		//int b = sony.getInfo("int"); // int 타입인 리턴값과 들어갈 변수의 타입은 같아야함.
		// 리턴 타입이 String[]인 메소드
		//String[] temp = sony.getInfo(args);
		
		
		
		
		
		
		

	}
}
