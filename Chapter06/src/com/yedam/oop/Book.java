package com.yedam.oop;

public class Book {	//하나의 설계도로 여러개 찍어내기
	
	//필드가 5개
	
//	 * 책 이름 : 이것이 리눅스다
//	 * # 내용
//	 * 1) 종류 : 학습서
//	 * 2) 가격 : 32000원
//	 * 3) 출판사 : 한빛미디어
//	 * 4) 도서번호 : yedam-002
//	 * 
	
	//필드
	
	String name;
	String content;
	String type = "학습서";			// 중복되는것은 필드로 저장, 나머지는 객체를 이용해서 생성.
	String price;
	String publisher;
	String isbn;
			
	//생성자
	public Book() {	// 기본 생성자(내용이 비어있는 생성자) , 객체에다가 바로 데이터를 넣을 필요없을때
		
	}
	
//	public Book(String name, String content) {
//		this name = name;
//		this
//	}
//	

	
	//메소드
	void getInfo() {
		System.out.println("책 이름 : "+ name);
		System.out.println("# 내용" + content);
		System.out.println("2) 가격 :" + price);
		System.out.println("3) 출판사 :" + publisher);
		System.out.println("4) 도서번호 :" + isbn);
	}
	
	
	
	
	
	
}
