package com.yedam.oop;

public class Application2 {

	public static void main(String[] args) {
		Calculator cl = new Calculator();  // cl : Calculator메소드를 이용하여 만든 객체
		
		int sumResult = cl.sum(1, 2); // 1과 2는 설계도의 sum 이라는 메소드에 대응되는 값을 지정하여 리턴
		
		double subResult = cl.sub(10, 20);
		
		System.out.println(sumResult);
		System.out.println(subResult);
		//String temp = cl.result("메소드 연습");
		//System.out.println(temp);
		//System.out.println(cl.result("메소드 연습"));	// 위 두 과정을 합친 것.
		cl.result("메소드 연습");	// 다른 공간 - 원하는 기능(함수: result) 가져와서 사용
		
		Computer myCom = new Computer();
		int result = myCom.sum(1,2,3);
		System.out.println(result);
		result = myCom.sum(1,2,3,4,5,6);
		System.out.println(result);

		
	
	
	
	}
	
}
// 객체 생성 -> 메서드를 가져와서(호출)해서 사용
// 자주 쓰는 기능 또는 다른 곳에 있는 기능을 불러와서 쓸때 따로 빼내서(만들어서) 메서드를 사용 // 재사용성 활용
// Ctrl+ 클릭 == F3 => 해당 메소드가 어디 있는지 보여줌, 이동 