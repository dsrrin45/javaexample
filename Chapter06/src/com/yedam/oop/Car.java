package com.yedam.oop;

public class Car {		// 복사만 활용
	
	// 필드
	String model;
	int speed;
	
	// 생성자
	Car(String model) {
		this.model = model;
	}
	// 메소드
	void setSpeed(int speed) {
		this.speed = speed;			//this는 mycar 안에 있는 인스턴스 필드를 의미.
	}
	
	void run() {
		// 반복문 생성
		// 조건 : i = 10부터 시작함
		// 	   : i는 50보다 작을때까지 반복해라
		//     : 반복문을 한번 실행하고 나면,, i의 값은 10씩 증가
		for(int i = 10; i<=50; i+=10) {
			this.setSpeed(i);		// 클래스 내부에서 메서드를 이용하는 법(this)
			System.out.println(this.model + "가 달립니다.(시속 :" + this.speed + "km/h");
			
		}
	}
	
	
	
	
	
	
	
	
}
