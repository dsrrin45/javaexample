package com.yedam.oop;

public class Student {
	// 생성자를 통한 필드 초기화
	
	// 필드
	String name;
	String school;
	String number;
	int kor;
	int math;
	int eng;
	
	
	// 생성자
	
	// 1) 생성자를 통한 필드 초기화	-> 생성자(매개변수)
	public Student(String name, String school, String number) {	// 생성자를 통해 데이터를 받음
		this.name = name;
		this.school = school;
		this.number = number;
		
	}
	// 2) 객체 필드에 접근하여 필드 초기화 	-> 기본생성자
	public Student() {
		
	}

	// 메소드
	   // 학생의 이름 : 김또치
	   // 학생의 학교 : 예담고등학교
	   // 학생의 학번 : 221126
	void getInfo() {
		System.out.println("학생의 이름 : "+ name);
		System.out.println("학생의 학교 " + school);
		System.out.println("학생의 학번: " + number);
		System.out.println("총 점 : " + sum());
		System.out.println("평 균 : " + avg());
	
	}
	
	int sum() {
		return kor+eng+math;	//결과값 반환		// 외부에서 가져와 씀.. 외부변수에 입력할 것이니까
	}							//외부(main문-객체화시키는 곳)에서 데이터를 입력 -> 데이터가 이미 들어 있는 상태 -> 바로 호출
	
	double avg() {
		double avgs = sum() / (double)3;		//sum()을 가져와 씀(재사용 가능)
		return avgs;			//결과값 반환
	}
	
	
	
}
