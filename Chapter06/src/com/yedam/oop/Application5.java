package com.yedam.oop;

public class Application5 {
	public static void main(String[] args) {
		
//		-> 객체 배열
//
//		int [] intAry = new int[6];		각 방에 숫자를 넣음
//		student[] stdAry = new student[6]	각 방에 사람(객체)을 넣음..
//
//		실습)
//		하나의 객체만 쓰는 게 아니고 여러개의 객체를 모아써 사용하는 것.
//		
		// 데이터를 가지고 생성자를 통한 6개의 객체 생성
		Student std1 = new Student("김또치", "예담고", "221124");
		Student std2 = new Student("이또치", "예담고", "221125");
		Student std3 = new Student("박또치", "예담고", "221126");
		Student std4 = new Student("최또치", "예담고", "221127");
		Student std5 = new Student("정또치", "예담고", "221128");
		Student std6 = new Student("장또치", "예담고", "221129");
		// 배열에 넣어서 써보기
		// int[] intAry = new int[6];
		// intAry[0] = 1;
		Student[] stdAry = new Student[6];	//Student 클래스 타입의 배열
		stdAry[0] = std1;
		stdAry[1] = std2;
		stdAry[2] = std3;
		stdAry[3] = std4;
		stdAry[4] = std5;
		stdAry[5] = std6;
		
		for(int i=0; i<stdAry.length; i++) {
			stdAry[i].kor = 50;
			stdAry[i].math = 60;
			stdAry[i].eng = 100;
			stdAry[i].getInfo();
		}


		
	}
}
