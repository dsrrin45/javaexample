package com.yedam.access;

public class Member {
	
	//필드
	private String id;
	private String pw;
	private String name;
	private int age;
	//생성자
	
	//메소드
	//setter, getter -> 데이터의 무결성을 지키기 위해서
	public void setAge(int age)	{	// 1.말도 안되는 데이터를 막기위해
		if(age < 0 ) {
			System.out.println("잘못된 나이입니다.");
			//this.age = 0;		// 1. 아예 설정해주는 방법
			return;	// 2. return;	-> return;을 만나면 하던 것을 멈추고 
					// 메소드를 바로 종료 한후, 메소드 호출한 곳으로 이동.
		}else {
			this.age = age;
		}
		System.out.println("return 적용 안됨.");
	}
	
	public int getAge() {		// 데이터를 가져올때, 값을 바꿔서(가공해서) 가져올때
		// 미국의 나이와 한국의 나이는 한 살 차이 나므로
		// 아래 내용을 실행한다.
		age = age + 1;
		return age;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {	// 2. 정한 기준에 미달하는지 확인하는 용도로 사용 가능.
		if(id.length() <= 8) {
			System.out.println("8글자보다 부족합니다. 다시 입력해주세요");
			return;
		}
		this.id = id;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
