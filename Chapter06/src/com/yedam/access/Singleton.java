package com.yedam.access;

public class Singleton {

	//정적 필드
	// 단 하나의 객체를 생성
	private static Singleton singleton = new Singleton();
	// 아무도 접근할 수 없는 자기자신의 객체를 만듬
	// 객체에다가 주소값이 들어감
	// 하나의 객체만 가지고 사용하게 금 public 으로 메소드를 만듬
	// getInstance를 가져오겟다는 뜻으로 메소드이름을 설정
	//객체를 막불러도 같은애가 나옴.
	// 생성자
	
	private Singleton() {
		
	}
	// 정적 메소드
	public static Singleton getInstance() {
		return singleton;
	}
	
	
}
