package com.yedam.access;
										// 4주차 22.11.25
public class Access {				// < 접근 제한자 실습 >
	/*
	 * public 어디서든 누구나 다 접근 가능
	 * protected 상속 받은 상태에서 부모-자식 간에 사용(패키지가 달라도 사용가능)
	 * 			패키지가 다른 사용 못함, 같은 패키지에서만 사용 가능
	 * default 패키지가 다르면 사용못함, 같은 패키지에서만 사용 가능
	 * private 내가 속한 클래스에서만 사용 가능
	 */
	// 접근제한자 -> 이름을 지어서 사용(변수, 클래스, 메소드 등등)에서 다 됨.
	//필드
	public String free;
	protected String parent;
	private String privacy;
	String basic;		// default 접근제한자
	
	
	// 생성자			// 자동완성해서 생성자 만듦
					//객체를 사용하려면 접근이 가능해야함 -> public	
	public Access() {
		
	}	//객체를 만들려면 생성자를 통해 만들어야 되는데 생성자에 접근가능해야함.
	
	
//	private Access(String privacy) {	//노란줄 경고창
//		this.privacy = privacy;
//	}
	
	// 메소드
//	public void run() {
//		System.out.println("달립니다");
//	}
	
	public void free() {
		System.out.println("접근이 가능합니다.");
		privacy();		// -내부에서는 안에서 사용 가능-
						// 클래스 내부에서 접근은 가능하니까(=실행은 가능) 
						// private 내부 내용 특허 기술들을 숨기면서 사용가능. <정보은닉:캡슐화>
	}
	private void privacy() {
		System.out.println("접근이 불가능합니다.");
	}
	
	
	
	
}
