package com.yedam.access;			// < 접근 제한자 실습 >
									//4주차 22.11.25
public class Application {
	public static void main(String[] args) {
		Access access = new Access();
		
		//public
		access.free = "free";
		
		//protected
		access.parent = "parent";
		
		//private
//		access.privacy = "privacy";
		
		//default
		access.basic = "basic";
		
		access.free();
		//access.privacy();
		
		Singleton obj1 = Singleton.getInstance();
		Singleton obj2 = Singleton.getInstance();
		// 새로운 변수나 객체를 만들어도, 같은 변수(=주소값이 같은)가 넘어옴
		
		if(obj1 == obj2) {
			System.out.println("같은 싱글톤 객체 입니다.");
		}else {
			System.out.println("다른 싱글톤 객체입니다.");
		}
	}
}
