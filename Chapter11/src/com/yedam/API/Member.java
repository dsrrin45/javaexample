package com.yedam.API;

public class Member {
	//String.equals()
	
	public String id;
	public String ssn;
	public String name;
	//객체.필드
	//객체.getter
	//객체.method
	//객체.toString()
	
	
	
	public Member(String id) {
		this.id = id;
	
	}

	@Override
	public String toString() {
		return "Member [id=" + id + ", ssn=" + ssn + ", name=" + name + "]";
	}

	//객체 동등 비교
	@Override
	public boolean equals(Object obj) {	//데이터를 비교하게끔 오버라이딩
		//Object obj -> 모든 클래스를 자동타입변환을 할 수 있는 그릇
		//어떤 클래스든 obj의 매개변수에 담을 수 있다.
		
		if(obj instanceof Member) {
			//obj가 Member 타입으로 자동타입변환되었는지 객체비교
			//Member타입으로 강제 타입 변환이 가능.
			//Member 클래스에 존재하는 필드와 메소드를 사용 할 수 있다.
			Member member = (Member) obj; // 스트링에 재정의한 equals를 가져옴?
			if(id.equals(member.id)) {	//강제타입변환 (obj에 있는 내용만 쓸수 있어서 자식클래스의 내용을 쓰려고 강제타입변환)
				return true; // 동등 객체로 보겠다.
			}
		}
		return false;
	}
	
}
