package com.yedam.API;
														//5주차 22.12.01 목
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
///java.util패키지에 있는 클래스 활용.
public class DateExample {

	public static void main(String[] args) {
		//날짜를 가져오기 위해서 Date 클래스 호출
		Date now = new Date();
		System.out.println(now.toString());
		
		///내가 원하는 포맷으로 바꿔줄 수 있다. -> 영어형태를 꼭 맞춰줘야함.
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy년 MM월 dd일 hh시 mm분 ss초");
		
		System.out.println(sdf.format(now));
		
		//Calendar 클래스
		
		Calendar now2 = Calendar.getInstance();	///싱클톤으로 단 하나의 객체(시간)를 만들어줌
		
		
		///객체를 통해서 Calendar클래스를 사용(가져올 수 있음)
		int year = now2.get(Calendar.YEAR);
		int month = now2.get(Calendar.MONTH)+1;	///월 0~11월 -> 1~12로 변경
		int day = now2.get(Calendar.DAY_OF_MONTH);
		
		int week = now2.get(Calendar.DAY_OF_WEEK);  /// 요일을 숫자로 받아옴
		
		String strWeek = null; /// -> 숫자로 받아옴 요일을 문자로 치환!
		
		switch (week) {
		case Calendar.MONDAY:
			strWeek = "월";
			break;
		case Calendar.TUESDAY:
			strWeek = "화";
			break;
		case Calendar.WEDNESDAY:
			strWeek = "수";
			break;
		case Calendar.THURSDAY:
			strWeek = "목";
			break;
		case Calendar.FRIDAY:
			strWeek = "금";
			break;
		case Calendar.SATURDAY:
			strWeek = "토";
			break;

		default:
			strWeek = "일";
			break;
		}
		
		
		int amPm = now2.get(Calendar.AM_PM);
		String strAmpm = "";
		
		if(amPm == Calendar.AM) {
			strAmpm = "오전";
		} else {
			strAmpm = "오후";
		}
		
		int hour = now2.get(Calendar.HOUR);
		int minute = now2.get(Calendar.MINUTE);
		int second = now2.get(Calendar.SECOND);
		
		System.out.println(year + "년");
		System.out.println(month + "월");
		System.out.println(day + "일");
		System.out.println(strWeek + "요일");
		System.out.println(strAmpm + " ");
		System.out.println(hour + "년");
		System.out.println(minute + "분");
		System.out.println(second + "초");

		
		
		
	}

}
