package com.yedam.API;

import java.io.UnsupportedEncodingException;

public class StringAPi {

	public static void main(String[] args)  {
		//String API ☆★☆★☆★☆★
		//byte[] -> String 변환
		// 숫자를 문자로 치환한 다음, 문자열로 바꾸는 것.
		
		byte [] bytes = {72,101,108,108,111,32,74,97,118,97};
		
		String str1 = new String(bytes);
		System.out.println(str1);
	
		//내가 원하는 위치를 선정하고(6) 그 갯수만큼 출력(4)
		String str2 = new String(bytes, 6, 4); //6에서부터 4개출력
		System.out.println(str2);
	
		//charAt()
		//인덱스 값(문자 위치)를 입력해서 해당 위치에 있는 문자를 가져 오는것.
		String ssn = "010624-1230123";
		char gender = ssn.charAt(7);
		switch (gender) {
		case '1':
		case '3':
			System.out.println("남자입니다.");
			break;
		case '2':
		case '4':
			System.out.println("여자입니다.");
			break;
		}
		
		
		//문자열 비교 ( equals())
		
		String str5 = "김또치";
		String str4 = "김또치";
		String str3 = new String("김또치");
	
		if(str4.equals(str5)) {
			System.out.println("같은 문자열");
		}else {
			System.out.println("다른 문자열");
		}
		
		if(str3.equals(str4)) {
			System.out.println("같은 문자열 ");
		}else {
			System.out.println("다른 문자열");
		}
		
		// 바이트 배열로 변환 String -> byte[]
		//encoding , decoding 
		//String -> byte : encoding 
		//byte -> String : decoding
		String str6 = "안녕하세요";
		
		byte[] bytes1 = str6.getBytes();	//타입 선언
		
		System.out.println("bytes1.length : " + bytes1.length);
		
		String str7 = new String(bytes1);
		System.out.println("bytes1 -> str6 : " + str7);
		
		try {
			byte[] bytes2 = str6.getBytes("EUC-KR");
			System.out.println("byte2.length : " + bytes2.length);
			String str8 = new String(bytes2, "EUC-KR");
			System.out.println(str8);
			
			byte[] bytes3 = str6.getBytes("UTF-8");	//타입 선언
			System.out.println("bytes3.length : " + bytes3.length);
			String str9 = new String(bytes3,"UTF-8");	//타입 선언
			System.out.println(str9);
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}//타입 선언
		
		//문자열 찾기
		// 문자열이 몇번째 위치에 존재하는지 확인 = indexOf()
		String subject = "자바 프로그래밍"; 
		int index = subject.indexOf("그래밍"); // 시작하는 위치를 찍어냄
		System.out.println(index);
		index = subject.indexOf("가");	// *없는 문자열이면 -1을 반환함.
		System.out.println(index);
		
		//문자열 길이
		
		System.out.println(subject.length()); //뛰어쓰기 포함!!
		
		//주민등록번호 - 13자리가 되어야 기본적으로 갯수.
		
		String ssn2 = "123456-1234567";
		if(ssn2.length() ==13) {
			System.out.println("주민번호 자리수가 맞습니다.");
		}else {
			System.out.println("주민번호 자리수가 틀립니다.");
		}
		
		// 문자열 대체  (문자열 바꾸기)
		String oldStr = "자바프로그래밍";
		String newStr = oldStr.replace("자바", "JAVA");
		System.out.println(newStr);
		
		//문자열 자.르.기
		//매개변수에 따라서 자르는 방법 다름.
		//1) 매개변수가 1개 일 때 - 해당 위치로 부터(바로뒤부터) 뒤로 다 짜르시오.
		//String ssn = "123456-1234567"
		String firstNum = ssn2.substring(7); 	
		System.out.println(firstNum);
		//2) 매개변수가 2개 일때 - 시작 위치, 끝나는 위치(끝나는 위치 앞까지)
		String secondNum = ssn2.substring(0,6);	// 6(끝나는 위치제외)을 포함하지 않고 짜름
		System.out.println(secondNum);
		
		
		// 대문자 -> 소문자
		 System.out.println("ABCDEFGHIJ".toLowerCase());
		
		 //소문자 -> 대문자
		 System.out.println("abcdefghij".toUpperCase());
		
		 //앞 뒤 공백 제거
		 System.out.println("    고희동   ".trim());
		 
		 // 기본타입 -> 문자열 변환
		 String temp = String.valueOf(123);
		 temp = String.valueOf(true);
		 System.out.println(temp);
		
		//문자열 분리하기
		//구분자를 통한 문자열 분리
		 String value = "1,2,3,4,5,6,7,8,9,10";
		 //spilt

		 String[] strAry = value.split(",");
		 
		 for(int i=0; i<strAry.length; i++) {
			 System.out.println(strAry[i]);
		 }
		 
		//StringBuilder - 하나의 메모리로 문자열을 수정.
		 //StringBuffer - 자매품
//		 String aa = "aaa";
//		 aa = aa + "bb"; // String은 플러스 연산자를 사용하면 새로운 문자열이 새로운 메모리영역에 다시 만듬
//		 
		 StringBuilder sb =new StringBuilder();
		 sb.append("예");
		 sb.append("담");
		 System.out.println("StringBuilder : "+sb);
		 
		 //concat - 문자열 합치기
		 //자매품 - +, append()
		 String var ="또치";
		 System.out.println(var.concat("김"));
		 
		 //contains : 포함하고 있는 문자열 확인(true, false)
		 if(var.contains("또치") ) {
			 System.out.println("또치를 포함 하고 있다");
		 } else {
			 System.out.println("또치를 포함 하지 않고 있다.");
		 }
			 
		//isEmpty : 문자열이 비어있는지 확인(데이터의 존재유무 확인-true, false)
		 
		 String empty = "";
		 if(empty.isEmpty()) {
			 System.out.println("문자열이 비었습니다.");
		 } else {
			 System.out.println("문자열이 비어있지 않습니다.");
		 }
		
		
		
		
		
	
	}

}
