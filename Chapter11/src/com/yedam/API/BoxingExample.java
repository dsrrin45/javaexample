package com.yedam.API;					//5주차 22.12.01 목

public class BoxingExample {

	///기본타입을 객체로 바꾸고 박싱 언박싱을 통해 왔다갔하는 방법
	/// 포장 클래스 이용.
	public static void main(String[] args) {
		//Boxing 기본 타입 -> 객체로 포장
		//Boxing
		Integer obj1 = new Integer(100);	/// 사용은 가능 권장하지 않는다는 중간줄.
		Integer obj2 = new Integer("200");
		Integer obj3 = new Integer("300");
		Integer obj4 = Integer.valueOf(100);
		Integer obj5 = Integer.valueOf("400");
		///boxing 방법
		/// 1. new연산자 활용
		/// 기본타입.valueOf()활용
		
		
		//unBoxing
		int value1 = obj1.intValue();
		int value2 = obj2.intValue();
		int value3 = obj3.intValue();
		int value4 = obj4.intValue();
		int value5 = obj5.intValue();

		
		System.out.println(value1);
		System.out.println(value2);
		System.out.println(value3);
		System.out.println(value4);
		System.out.println(value5);

		///어떤데이터가 들어오든 언박싱으로 숫자로 변환해서 데이터를 가져옴.
		///기본타입Value(); -> 언박싱하는 법...
		
		// 자동 박싱
		Integer obj6 = 100;
		System.out.println("value : " + obj6.intValue());
		
		//대입 시 자동 언박싱
		int value6 = obj6; ///객체안의 숫자가 자동으로 언박싱되어 들어감.
		System.out.println("value : " + value6);
		
		//연산 시 자동 언박싱
		int value7 = obj6 + 300;
		System.out.println("value : " + value7);
		
		// 포장 값 비교
		Integer obj7 = 100;
		Integer obj8 = 100;
		
		System.out.println(obj7 == obj8); /// ==는 데이터를 비교하는 것이 아니고 주소값을 비교하게됨
		System.out.println(obj7.intValue() == obj8.intValue());
		///다만, 미리 정의된 내용의 범위의 값일 경우 비교 가능.(p.503참고)
		
		Boolean obj9 = true;
		Boolean obj10 = true;
		
		System.out.println("Boolean 비교 : " + (obj9 == obj10));
		
		Byte obj11 = -100;
		Byte obj12 = -100;
		
		System.out.println("Byte 비교 : " + (obj11 == obj12));
		
		
		
		
		
		
		
	}

}
