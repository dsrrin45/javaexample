package com.yedam.API;

import java.util.Scanner;

public class StringStudy {

	public static void main(String[] args) {

		// 1. 문자열 뒤집기	// 반복문 조건...
		String str = "123456789"; //987654321
				
		for(int i = str.length()-1; i>=0; i--) {
			System.out.print(str.charAt(i));
		}
		
		
		System.out.println();
		
		
		// 2. 세 개의 임의 단어 3개 중 가장 짧은 단어와 길이 출력하기
		
		String [] sttr = {"꽃","나무","강아지"}; 
		//String str1 = "꽃";
		//String str2 = "나무";
		//String str3 = "강아지";
		
		//min 최소값
		
		String min = sttr[0];
	
		for(int i=0; i<sttr.length; i++) {
	
			if(min.length()>sttr[i].length()) {
				min = sttr[i];
			}
			
		}
		System.out.println("가장 짧은 단어 : " + min );
		System.out.println("가장 짧은 단어 길이: " + min.length());
		
		
		String firstWord = "abc";
		String secondWord = "abcd";
		String thirdWord ="abcde";
		String shortWord = firstWord + " : " + firstWord.length();
				
				if(firstWord.length() > secondWord.length()) {
					shortWord = secondWord + " : " + secondWord.length();
					if(secondWord.length() > thirdWord.length()) {
						shortWord = thirdWord + " : " + thirdWord.length();
					}
				} else {
					if(firstWord.length() > thirdWord.length() ) {
						shortWord = thirdWord + " : " + thirdWord.length();
					}
				}
				
		
		
		// 3. ID와 비밀번호를 입력받아 ID가 3글자 보다 크고 비밀번호가 8글자 이상 이면
		// 사용할 수 있는 ID와 PW입니다. 출력하기
		// 해당 아이디에 숫자 몇개, 문자 몇개, 특수문자 몇개
		
		Scanner sc = new Scanner(System.in);
		System.out.println("id 입력 > ");
		String id = sc.nextLine();
		System.out.println("pw 입력 > ");
		String pw = sc.nextLine();
		if(3<id.length() && pw.length()>=8) {
			System.out.println("사용할 수 있는 ID와 PW입니다.");
		}else {
			System.out.println("사용할 수 없는 ID와 PW입니다.");
		}
		
		
		// 4. 생년월일 입력 후 나이 출력하기(220101 -> 2022년생, 230202 -> 1923년생) 올해기준 100년 이내만
		// 예시)  입력 : 950101 -> 출력 : 28
		//		 입력 : 001013 -> 출력 : 23
		
		//System.out.println("생년월일 입력 > ");
		//String birth = sc.nextLine();
		
		//문자열 자르기
		//if
		
		// 1. 계산식 세워야한다.
		// 1-1. 00년 이후 출생한 사람의 나이 구하기 -> (22-00)+1
		// 1-2. 00년 이전 출생한 사람의 나이 구하기 -> (122-99)+1
		// 99 -> 24( x -99 = 24), x = 99+24, (122-생년월일)+1
		// 98 -> 25
		// 97 -> 26
		
		Scanner sc = new Scanner(System.in);
		System.out.println("생년월일 >");
		String birth = sc.nextLine();
		int birthNo = Integer.parseInt(birth.substring(birth.substring(0,2)));
		
		if(birthNo <= 22) {
			System.out.println("나이 : " + ((22-birthNo)+1));
		}else if(birthNo > 22 ) {
			System.out.println("나이 : " + (123-birthNo));
		}
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
