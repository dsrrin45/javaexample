package com.yedam.API;
												//5주차 22.12.01 목
///Math클래스를 사용하여 값 구하기
public class MathExample {

	public static void main(String[] args) {
		int v1 = Math.abs(-5); /// 5 abs는 절대값. 부호 상관없는것.
		double v2 = Math.abs(-3.14);
		System.out.println("v1 = " + v1);
		System.out.println("v2 = " + v2);
		
		double v3 = Math.ceil(5.3);
		double v4 = Math.ceil(-5.3);	///ceil는 올림.
		System.out.println("v3 = " + v3);
		System.out.println("v4 = " + v4);

		double v5 = Math.floor(5.3);	///floor는 내림.
		double v6 = Math.floor(-5.3);
		System.out.println("v5 = " + v5);
		System.out.println("v6 = " + v6);

		int v7 = Math.max(5, 9);
		double v8 = Math.max(5.3, 2.5);
		System.out.println("v7 = " + v7);
		System.out.println("v8 = " + v8);
		
		int v9 = Math.min(5, 9);
		double v10 = Math.min(5.3, 2.5);
		System.out.println("v9 = " + v9);
		System.out.println("v10 = " + v10);

		double v11 = Math.random();	///random값 0부터~1까지의 아무 값.
		System.out.println("v11 = " + v11);
		///rint 퐁당퐁당 반올림. 가까운 정수의 실수값.
		// 0~10 .5 애매한~ 앞의 정수가 짝수 : 내림 홀수: 올림
		// 		
		double v12 = Math.rint(5.5);	
		double v13 = Math.rint(6.5);
		System.out.println("v12 = " + v12);
		System.out.println("v13 = " + v13);

		///round 반올림값.
		double v14 = Math.round(5.3);
		double v15 = Math.round(5.7);
		System.out.println("v14 = " + v14);
		System.out.println("v15 = " + v15);


	}

}
