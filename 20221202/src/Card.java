
public class Card {

	//필드
	String cardNo;
	String validDate;
	String cvc;
	
	//생성자
	public Card(String cardNo, String validDate, String cvc ) {
		this.cardNo = cardNo;
		this.validDate = validDate;
		this.cvc = cvc;
	}
	
	public Card() {
		
	}

	public String getCardNo() {
		return cardNo;
	}

	public String getValidDate() {
		return validDate;
	}

	public String getCvc() {
		return cvc;
	}
	
	
	
	//메소드
	
	public void showCardInfo() {//정보출력하는 메서드
		System.out.println("카드정보 ( Card NO : " + cardNo + ", 유효기간 : " + validDate + ", CVC : " + cvc + " )");
	}

	public void showCardInfo(String cardNo, String cardStaff, String company) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	
	
	
	
}
