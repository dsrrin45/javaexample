
public class CardPayment implements Payment {

	double cardRatio;
	
	public CardPayment(double cardRatio) {
		this.cardRatio = cardRatio;
	}
	
	////계산식!!!!!! 생각하기
	
	@Override
	public int online(int price) {
		System.out.println("온라인 결제시 총 할인율 : " + Payment.ONLINE_PAYMENT_RATIO );
		return 0;
	}

	@Override
	public int offline(int price) {
		System.out.println("오프라인 결제시 총 할인율 : " + Payment.OFFLINE_PAYMENT_RATIO );
		return 0;
	}

	@Override
	public void showInfo() {
		System.out.println("*** 카드로 결제시 할인정보");
	}

}
