
public class CardTest {

	public static void main(String[] args) {

		Card card = new Card("5432-4567-9534-3657","20251203","253");
		
		card.showCardInfo();
		
		Card tossCard = new TossCard("5432-4567-9534-3657","신빛용","Toss");
	
		tossCard.showCardInfo();
		
		Card dgbCard = new DGBCard("5432-4567-9534-3657","20251203","253");
	
		dgbCard.showCardInfo();
		
//		Card dgbCard2 = new DGBCard("신빛용","대구은행");
//		
//		dgbCard2.showCardInfo();
	}

}
