package com.yedam.study;			// < 문제 접근 방법 >

import java.util.Scanner;

public class Application {
	public static void main(String[] args) {
		// 두개의 주사위를 던졌을때, 눈의 합의 6이 되는 모든 경우의 수를
		// 출력하는 프로그램 구현을 하시오.
		//===================================================
		// 모든 경우의 수 생각 6*6 = 36개 경우의 수 -> "중첩 For문"
		// 합이 6이 되는 경우를 생각 -> 규칙 찾기
		// 1, 5 / 2, 4/ 3,3 / 4,2 / 5,1
		// A주사위 B주사위
		// A주사위가 1일때((경우의 수를 생각!)), B 주사위가 1~6까지의 합을 구한 다음
		// 합의 결과가 6이면 -> 내가 원하는 조건의 만족
		
		
		// 숫자를 하나 입력 받아, 양수인지 음수인지 출력
		// 단 0이면 0입니다.라고 출력해주세요.
		
		Scanner sc = new Scanner(System.in);
		System.out.println("입력>");
		int num = Integer.parseInt(sc.nextLine());
		if(num>0) {
			System.out.println("양수");
		}else if(num<0) {
			System.out.println("음수");
		}else {
			System.out.println("0");
		}
		
		
		// 정수 두개와 연산 기호 1개를 입력 받아서
		// 연산 기호에 해당되는(==같은 곳을 찾아서) 계산을 수행하고 출력하세요.
		
		System.out.println("입력>");
		int num1 = Integer.parseInt(sc.nextLine());
		System.out.println("입력>");
		int num2 = Integer.parseInt(sc.nextLine());
		System.out.println("연산기호 입력>");
		//String cal = String.valueOf(sc.nextLine());
		String cal = sc.nextLine();
		
		if("cal") {
			
		}
		
		
		
	}

}
