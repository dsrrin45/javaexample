package com.yedam.study;			// 주사위 굴리기 시험

import java.util.Scanner;

public class Exam01 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int[] dice = null;
		int size = 0;
		boolean run = true;
		while(run) {
			System.out.println("===1. 주사위 크기 | 2. 주사위 굴리기 | 3. 결과보기 | 4. 가장 많이 나온 수 | 5. 종료 ====");
			System.out.println("메뉴 >");	
			String selectNo = sc.nextLine();
			
			
			switch(selectNo) {
			case "1": 
				System.out.println("주사위 크기>");
				size = Integer.parseInt(sc.nextLine());
				//if문 활용
				if(size < 5 || size > 10) {
					System.out.println("입력한 값의 범위를 벗어 났습니다." +
									"5~10 사이의 수를 입력해 주세요.");
				}
				break;
			case "2":
				//주사위 크기 설정
				dice = new int[size];
				int count = 0;
				//5가 나올때 까지 -> 언제까지 진행할지 조건을 알 수가 없습니다.
				while(true) {
					int random = (int)(Math.random() * size) +1;	//범위사이즈 만큼 랜덤값을 뽑기 위해
					// 각 숫자 나온 횟수 저장
					dice[random-1] = dice[random-1] +1;	//3이면 방번호 2에 넣어야하니까 +1은 몇번나오는지 저장.
					count++;
					if(random == 5) {	// 5가 나올때 까지 주사위를
						break;
					}
				}
				System.out.println("5가 나올때 까지 주사위를" + count + "번 굴렸습니다.");
				break;
			case "3":
				for(int i=0; i<dice.length; i++) {
					System.out.println((i+1)+ "은" + dice[i]+"번 나왔습니다.");
				} // 1을 표현하기 위해 방번호에 +1
				
				// 가장 많이 나온 수 구하기 
			case "4"://향상된 for문으로 하는 법
				int max = 0;
				int index = 0;
				for(int num : dice) {
					if(max<num) {
						max = num;
					}
				}
				
				//일반 for문 사용하는 법
				for(int i=0; i<dice.length; i++) {
					if(max<dice[i]) {
						index = i;
					}
				}
				System.out.println("가장 많이 나온 수는" + index + "입니다.");
				break;
				
			case "5":
				System.out.println("프로그램 종료");
				run=false;
				break;
			}
			
			
			
		}
		
		
		
	}

}
