package com.yedam.excep;
											//5주차 22.11.30 수
public class Application2 {				
	public static void main(String[] args) {
		// 예외를 호출한 곳으로 떠넘김.
		try {
			findClass();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();	//오류난걸 캐치해서 적어주는것
			System.out.println(e.getMessage()); // 무엇때문에 오류났는지 설명해주는 것.
			System.out.println("클래스가 존재하지 않습니다.");
		}
	}
	
	public static void findClass() throws ClassNotFoundException {
		Class clazz = Class.forName("java.lang.String2");
	}
}
