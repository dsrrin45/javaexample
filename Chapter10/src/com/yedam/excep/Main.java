package com.yedam.excep;

public class Main {

	public static void main(String[] args) {

		//nullpointexception
		// 객체에 데이터가 들어가지않았을 때 오류
//		String data = null;
//		System.out.println(data.toString());
//		
//		Example example = null;
//		System.out.println(example.toString());
		
		//ArrayIndexOutOfBoundsException
		//배열의 범위에서 벗어난 인덱스를 호출할 때 발생하는 예외
//		int[] intAry = new int[3]; // 0~2 index(방 번호)
//		
//		intAry[0] = 1;
//		intAry[1] = 2;
//		intAry[2] = 3;
//		
//		intAry[10] = 4;
		
		//NumberFormatException
		//숫자로 바꿀수 없는 문자를 숫자로 바꾸려고 할 때 나는 오류
		String str = "123";
		int val = Integer.parseInt(str);
		System.out.println(val);
		//예외 케이스
//		String str2 = "자바";
//		val = Integer.parseInt(str2);
//		System.out.println(str2);
//		String str3 = "";	// 비워있는 공간에도 예외 발생.
//		val = Integer.parseInt(str3);
//		System.out.println(str3);
		
		//ClassCastException
		// 자동타입변환 된 객체를 강제 타입변환 할 때 발생 예외
		
		//정상 케이스
		//자동타입변환
		Example exam = new Exam();
		//강제타입변환
		Exam exam2 = (Exam) exam;
		
		//예외 케이스 -> instanceof로 해결
		//Example 자신의 객체를 만듬.
		Example exam3 = new Example();
//		//자신의 객체를 자식으로 바꾸는 과정
//		Exam exam4 = (Exam)exam3;
		
		//예외 처리
		//exam3과 Exam이 같은 객체인지 확인 하고 나서
		//강제 타입 변환
		if(exam3 instanceof Exam) {
			Exam exam5 = (Exam)exam3;
		}
		//ClassNoteFoundExcpetion - class파일 찾지 못할 때(실행메소드 못 찾을 때 자주 발생) 
		//AirthmeticException - 숫자를 0으로 나눌 때
		//OutOfMemoryError - 메모리가 부족 할 때(반복문의 종료를 하지못하고 무한 루프때 간혹 발생)
		//IOException - 입출력 오류
		//FileNotFoundException - 해당 경로 파일 찾지 못할 때
		
		
		
//		
	}

}
