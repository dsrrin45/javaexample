package com.yedam.excep;

public class Application {

	public static void main(String[] args) {
//		try {
//			//예외가 발생할 만한 코드
//		}catch() {
//			//예외가 발생 후 처리하는 코드
//		}
		// 1) 자바가 먼저 컴파일 후 인식해서 예외처리.
		try {
//			double avg = 1/0;
//			System.out.println(avg);
			
			//NumberFomat 예외 발생 지점
			String str = "자바";
			Integer.parseInt(str);
			System.out.println("변환완료!");
			
			//ClassNotFound 예외 발생 지점
			Class clazz = Class.forName("java.lang.String2");
			
			System.out.println("예외처리 발생!!");
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();	//예외가 발생한 경로를 추적하고 console에 출력하세요.
			System.out.println("catch문으로 이동");
			
		} catch(NumberFormatException e) {
			e.printStackTrace();
			System.out.println("NumberFomat 예외 발생");
			
		} catch(Exception e) {		// 예외처리의 부모격 클래스가 모든경우를 다확인함 -> 맨 밑에서 처리
			e.printStackTrace();
			System.out.println("Exception 발생!!");
			
		} finally {
			System.out.println("finally 항상 실행");
		}
		
		System.out.println("try-catch 탈출");
	}

}
