package com.yedam.inter;

public class RCExample {
	public static void main(String[] args) {
		RemoteControl rc;	// 인터페이스에서 객체를 만들수없기때문에 사용할수 있겟금 변수를 생성
		
		//자동타입변환
		//rc = new Television(); // 자식클래스를 가지고 객체화하면서 자식 클래스안에 재정의해놓은 메서드가 실행
		rc = new SmartTv();
		
		//SmartTv 클래스 -> implements RemoteControl( + Searchable)
		//RemoteControl(+Searchable) -> Searchable을 상속을 받고 있기 때문에
		//RemoteControl
		rc.turnOn();
		rc.setVolume(40);
		rc.turnOff();
		
		Searchable sc = new SmartTv();
		rc.search("www.google.com");
		
		rc = new Audio();
		
		rc.turnOn();
		rc.setVolume(5);
		rc.turnOff();
		
		
//		Television tv = new Television(); // 자기자신을 객체화해서 한 방법 추천안함
//		
//		tv.turnOn();
//		tv.setVolume(4);
//		tv.turnOff();
		
	}
}
