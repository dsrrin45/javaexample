package com.yedam.inter;

public class Television	implements RemoteControl {

	//필드
	private int volume;
	//생성자
	//메소드
	
	@Override
	public void turnOn() {
		System.out.println("전원을 켭니다.");
	}

	@Override
	public void turnOff() {
		System.out.println("전원을 끕니다.");
	}

	@Override
	public void setVolume(int volume) { // 볼륨을 설정하는 메소드
		//최대 소리 이상으로 데이터가 들어 올 때
		if(volume > RemoteControl.MAX_VOLUME) {
			this.volume = RemoteControl.MAX_VOLUME;
		}
		//최소 소리 이하로 데이터가 들어 올 때
		else if(volume < RemoteControl.MIN_VOLUME) {
			this.volume = RemoteControl.MIN_VOLUME;
		} else {
			this.volume = volume;	// 0~10사이의 정상데이터가 들어올때
		}
		System.out.println("현재 TV 볼륨 : " + this.volume);
	}

	@Override
	public void search(String url) {
		// TODO Auto-generated method stub
		
	}

}
