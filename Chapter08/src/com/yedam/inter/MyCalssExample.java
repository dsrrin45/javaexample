package com.yedam.inter;

public class MyCalssExample {
	public static void main(String[] args) {
		System.out.println("1)===============");
		
		MyClass myClass = new MyClass();
		
		myClass.rc.turnOn(); //myclass 필드 안에 있는 객체 rc안에 존재하는 turnOn 메서드를 실행.
		myClass.rc.turnOff();
	
		System.out.println();
		System.out.println("2)===============");
		
		MyClass myClass2 = new MyClass(new Audio()); // 생성자 매개변수에 인터페이스를 사용
		
		System.out.println();
		System.out.println("3)===============");
	
		MyClass myClass3 = new MyClass();
		myClass3.method1();
	
		System.out.println();
		System.out.println("4)===============");
		
		MyClass myClass4 = new MyClass();
		myClass4.methodB(new Television());
		
		
		
	
	}
}
