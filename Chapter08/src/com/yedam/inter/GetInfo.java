package com.yedam.inter;

public interface GetInfo {	// 인터페이스는 각각의 물체에 대한 공통의 기능을 모아놓은 것.
	
	void area();
	void round();

}
