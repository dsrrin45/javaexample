package com.yedam.inter;

public class Bird implements Animal{		// 자식 클래스

	@Override
	public void walk() {
		System.out.println("걸을 수 있음.");
	}

	@Override
	public void fly() {
		System.out.println("날 수 있음.");
		
	}

	@Override
	public void sing() {
		System.out.println("노래 할 수 있음.");
		
	}

}
