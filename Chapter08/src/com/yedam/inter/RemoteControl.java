package com.yedam.inter;

public interface RemoteControl extends Searchable {	//리모콘을 컨트롤하는 설계도
	
	//상수
	public static final int MAX_VOLUME = 10;
	public int MIN_VOLUME = 0; // 상수 선언 생략 가능
	
	// 추상 메소드
	public void turnOn();
	public abstract void turnOff();
	public void setVolume(int volume);
}
