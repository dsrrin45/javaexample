package com.yedam.inter;

public class WSExample {

	public static void main(String[] args) {
		WashingMachine LGws = new LGWashingMachine(); // 부모 //마지막 자식
		LGws.startBtn();
		LGws.pauseBtn();
		System.out.println("세탁기 속도: " + LGws.changeSpeed(2)
		+ "변경하였습니다.");
		LGws.startBtn();
		
		LGws.dry();
	}

}
