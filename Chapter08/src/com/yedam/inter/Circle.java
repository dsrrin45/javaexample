package com.yedam.inter;

public class Circle implements GetInfo{
	
	//필드
	int radius;	//데이터
	
	//생성자
	public Circle(int radius) {
		this.radius = radius;	// 생성자를 통해서 데이터를 받아옴.
	}

	@Override
	public void area() {
		//원 넓이 PI * R * R
		System.out.println(3.14 * radius * radius);
	}

	@Override
	public void round() {
		// 원둘레 2*PI*R
		System.out.println(2*3.14*radius);
	}

	
}
