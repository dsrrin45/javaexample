package com.yedam.inter2;

public class Application {

	public static void main(String[] args) {
		Vehicle v1 = new Bus(); // 자동타입변환
//		drive(v1);
//		
//		Vehicle v2 = new Taxi();
//		drive(v2);

		v1.run();
//		v1.checkFare();// 자동타입변환한 부모에게 없어서
		
		Bus bus = (Bus) v1; // 강제 타입변환 // 캐스팅
		
		bus.run();
		bus.checkFare();
		
		drive(new Bus());
		drive(new Taxi());
	}
	
	
	public static void drive(Vehicle vehicle) {	// = Vehicle vehicle = new bus();
		if(vehicle instanceof Bus) {
			Bus bus = (Bus) vehicle;
			bus.checkFare();
		}
		vehicle.run();
		
		
		
		
		
		vehicle.run();
	}

	
	
	
	
}
