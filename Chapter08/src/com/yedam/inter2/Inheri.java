package com.yedam.inter2;

public class Inheri {
	public static void main(String[] args) {
		// A <- B <- D( + A <- D)
		// A <- B
		A a = new B();
		a.info();
		// A <- D
		A a2 = new D(); // B의 정의해놓은 내용을(상속) 가져와서 실행가능.
		a2.info();
	}
}
