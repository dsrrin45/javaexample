package com.yedam.inter2;

public class Car {
	//인터페이스는 자기자신을 객체로 못만들기 때문에, 자식클래스를 활용해서 만들어서, 자동차에다가 붙여줌
	Tire frontLeftTire = new HankookTire();
	Tire frontRightTire = new KumhoTire();  
	Tire backLeftTire = new HankookTire();
	Tire backRightTire = new KumhoTire();
	
	public void run() {
		frontLeftTire.roll();
		frontRightTire.roll();
		backLeftTire.roll();
		backRightTire.roll();
	}
}
