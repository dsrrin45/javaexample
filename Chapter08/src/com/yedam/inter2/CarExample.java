package com.yedam.inter2;

public class CarExample {	//인터페이스에 필드의 다형성 활용 예제

	public static void main(String[] args) {
		Car myCar = new Car();
		
		myCar.run();
		System.out.println("====================");
		//타이어 교체
		myCar.frontLeftTire = new KumhoTire();
		myCar.backRightTire = new HankookTire();
		
		myCar.run();
	}

}
