package com.yedam.inter2;

public class Example {

	public static void main(String[] args) {
		ImplementC imp1 = new ImplementC();
		//바라보는 곳에 따라 불러낼 수 있는 것들이 달라짐
		InterfaceA ia = imp1;	//ImplementC()는 a의 자식이기때문에 가능
		ia.methodA();
		
		System.out.println();
		
		InterfaceB ib = imp1; //ImplementC()는 b의 자식이기때문에 가능
		ib.methodB();
		
		System.out.println();
		////ImplementC()는 c의 자식이기때문에 가능
		// InterfaceC는 a,b의 자식이기때문에 가능
		InterfaceC ic = imp1; 
		ic.methodA();
		ic.methodB();
		ic.methodC();
		
	}

}
