package com.yedam.arrange;   			// 4주차 22.11.23

public enum Week { // 자바에서 상수는 대문자!
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY,
	SUNDAY
}
