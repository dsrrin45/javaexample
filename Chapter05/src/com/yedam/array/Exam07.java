package com.yedam.array;				// 4주차 22.11.23

public class Exam07 {
	public static void main(String[] args) {
		int[][] intAry = {{1,2} , {1,2,3}};
		
		//						{{1,2,3}, {1,2,3}}
		int[][] mathScore = new int[2][3]; // 2가지 덩어리에 데이터 3개
		// 2차원 배열 - 이중 포문 사용
		// 첫번째 문(배열)을 여는 반복문
		for(int i = 0; i<mathScore.length; i++) {
			System.out.println(mathScore.length); // 첫번째 배열값 2
			//두번째 문(배열안의 배열)을 여는 반복문
			for(int k=0; k<mathScore[i].length; k++) {
				
				System.out.println("mathScore["+i+"]["+k+"]="
						+ mathScore[i][k]);
			}
		}
	}
}
