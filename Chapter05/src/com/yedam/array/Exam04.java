package com.yedam.array;				// 4주차 22.11.23

import java.util.Scanner;

public class Exam04 {
	public static void main(String[] args) {
		//Lotto - 보류
		
		// 배열에 담긴 값 중에서 최대값, 최소값 구하기
		
	Scanner sc = new Scanner(System.in);
			
			int[] ary;
			int no;
			
			System.out.println("배열의 크기>");
			no = Integer.parseInt(sc.nextLine());
			ary = new int[no];
			
			for(int i=0; i<ary.length; i++) {
				System.out.println("입력>");
				ary[i] = Integer.parseInt(sc.nextLine());
			}
			
			// 최대값 구하기
			int max = ary[0];
			for(int i=0; i<ary.length; i++) {
				if(max < ary[i]) { // 내가 가지고 있는 데이터 하나씩 가져온 다음에 비교
					max = ary[i];
				}
			}
			
			System.out.println("최대값 : " + max);
			
			// 최소값 구하기
			int min = ary[0]; // 비교 대상을 첫번째 요소로
			for(int i=0; i<ary.length; i++) {
				if(min > ary[i]) {
					min = ary[i];
				}
			}
			System.out.println("최소값 : " + min);
		
	}
}
