package com.yedam.homeworks.src.com.yedam.homework;

public abstract class Culture {

	String title;
	int director;
	int actor;
	int audience;
	int total;
	
	public void Culture(String title,int director,int actor) {
		this.title = title;
		this.director = director;
		this.actor = actor;
	}
	
	//관객수와 총점을 누적시키는 기능
	public void setTotalScore(int score) {
		//관객 수 1씩 증가
		this.audience++;
		//점수를 누적(총점)
		this.total += score;
	}
	//평점을 구하는 기능
	public String getGrade() {
		int avg = total / audience;
		
		String grade = "";
		
		switch (avg) {
		case 1: 
			grade = "☆";
			break;
		case 2: 
			grade = "☆☆";
			break;
		case 3: 
			grade = "☆☆☆";
			break;
		case 4: 
			grade = "☆☆☆☆";
			break;
		case 5: 
			grade = "☆☆☆☆☆";
			break;
		
		}
		// 2번째 방법
//		for(int i =0; i<avg; i++) {
//			grade += "☆";
//		}
//		
		return grade;
		
	}
	//정보를 출력하는 추상메소드
	public abstract void getInformation();
	
	
	
	
	
	
}
