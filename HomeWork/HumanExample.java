package com.yedam.homeworks.src.com.yedam.homework;

import com.yedam.homework.ObesityInfo;

public class HumanExample {

	public static void main(String[] args) {

		Human human = new StandardWeightInfo("홍길동",168,45);
				
		human.getInformation();
		
		
		human = new ObesityInfo("박둘이", 168, 90);
		
		human.getInformation();
	}

}
