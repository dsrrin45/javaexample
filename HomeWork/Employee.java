package com.yedam.homeworks.src.com.yedam.homework;

public class Employee {

	//필드
	private String empName;
	private int salary;
	
	// 생성자
	public Employee(String empName, int salary) {
		this.empName = empName;
		this.salary = salary;
	}
	
	//메소드
	
	public void getInformation() {
		System.out.printf("이름 : " + this.empName + " " + "연봉 : " + this.salary);
		//System.out.println("이름 : " + this.empName + "연봉 : " + this.salary);

	}
	
	public String getEmpName() {
		return empName;
	}

	public int getSalary() {
		return salary;
	}

	public void print() {
		System.out.println("수퍼클래스");
	}
}
