package com.yedam.homeworks.src.com.yedam.homework;

public class StandardWeightInfo extends Human {
	//필드
	double standardWeight;
	//생성자
	public void StandardWeightInfo(String humanName,double height,double weight,double standardWeight) {
		super(humanName,height,weight);
		//this.standardWeight = standardWeight;
	}
	
	
//	StandardWeightInfo 클래스를 정의한다.
//	- Human 클래스를 상속한다.
//	- 메소드는 다음과 같이 정의한다.
//	(1) public void getInformation() : 이름, 키, 몸무게와 표준체중을 출력하는 기능
//	(2) public double getStandardWeight() : 표준체중을 구하는 기능
//	( * 표준 체중 : (Height - 100) * 0.9 )
//	
//	
	//메소드
		
	@Override
	public void getInformation() {
		super.getInformation();
		//1번 방식
		System.out.println(" 표준체중" + standardWeight + "입니다");
		//2번 방식
		System.out.printf("표준체중 %.1f 입니다.\n", getStandardWeight());
	}
	
	public double getStandardWeight() {
		double stdWeight = (height -100) * 0.9;
		return stdWeight;	// 결과값 반환 -> 원래 자리로 돌아간다.
	}
	

}
