package com.yedam.homeworks.src.com.yedam.homework;

public class Human {

//	1) Human 클래스를 정의한다.
//- 이름과 키, 몸무게를 필드로 가지며 생성자를 이용하여 값을 초기화한다.
//- 메소드는 다음과 같이 정의한다.
//(1) public void getInformation() : 이름, 키와 몸무게를 출력하는 기능
	
	
	//필드
	String humanName;
	double height;
	double weight;
	//생성자
	public Human(String humanName,double height,double weight) {	// 생성자를 이용하여 값을 초기화
		this.humanName = humanName;
		this.height = height;
		this.weight = weight;
	}
	
	//홍길동님의 신장 168, 몸무게 45, 표준체중 61.2 입니다.
			//박둘이님의 신장 168, 몸무게 90, 비만입니다.
	
	//메소드
	public void getInformation() {	// 이름, 키, 몸무게 출력하는 기능
		System.out.printf("이름 : " + humanName + " 키 : " + height+" 몸무게 :" +  weight );
	}
}
