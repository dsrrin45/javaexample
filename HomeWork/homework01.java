package com.yedam.homework;

public class homework01 {

	
	public static void main(String[] args) {
		// 문제 1
		byte num1 = 37;
		int num2 = 91;
		System.out.println("num1: "+ num1 + " , "+ "num2 :"+ num2);
		
		// 문제 2
		int result11 = num2 + num1;
		int result12 = num2 - num1;
		int result13 = num2 * num1;
		int result14 = num2 / num1;
		
		System.out.println(result11);
		System.out.println(result12);
		System.out.println(result13);
		System.out.println(result14);
		
		// 문제 3
		short var1 = 128;
		System.out.printf( var1 + ", short\n");
		
		String var2 = "B";
		System.out.printf(var2 + " , String\n");
//				
		int var3 = 44032;
		System.out.printf(var3 + ", int\n");
		
		long var4 = 100000000000L;
		System.out.printf(var4 + " , long\n");
		
		float var5 = 43.93106F;
		System.out.printf(var5 + " , float\n");
		
		float var6 = 301.3F;
		System.out.printf(var6 + " , float\n");
		
		// 문제 4
		byte a = 35;
		byte b = 25;
		int c = 463;
		long d = 1000L;
		// 4-1
		long result1 = a + c + d;
		System.out.println(result1);
		// 4-2
		int result2 = a + b + c;
		System.out.println(result2);
		// 4-3
		double e = 45.31;
		double result3 = c + d + e;
		System.out.println(result3);	
		
		// 문제 5 A278번지10.0 
		int intValue1 = 24;
		int intValue2 = 3;
		int intValue3 = 8;
		int intValue4 = 10;
		char charValue = 'A';
		String strValue = "번지";
		
		int iResult = intValue1 + intValue2;
		//System.out.printf("%s%d%d%2s%3.1f",charValue, iResult, intValue3, strValue, (double)intValue4);
		System.out.println(String.valueOf(charValue) + (intValue1 + intValue2) + intValue3 + strValue + (double)intValue4);

		// 추가 문제
		int value = 485;
		
		int hundred = value / 100; // 4
		int ten  = (value - (100*hundred)) / 10; // 8
		int one = (value - (100*hundred) - (10*ten)); // 5
		
		int intResult = hundred + ten + one;
		System.out.println(intResult);
	}

}
