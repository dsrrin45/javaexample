package com.yedam.homeworks.src.com.yedam.homework;

public class EmpDept extends Employee {
	// 필드
	String deptName;

	public String getDeptName() {
		return deptName;
	}

	public EmpDept(String empName, int salary, String deptName) { // 생성자를 맞춰져야 상속올바르게 이루어짐
		super(empName, salary);
		this.deptName = deptName;
	}

	@Override
	public void getInformation() {
		super.getInformation();	// 부모의 메소드를 재실행(재사용)
		System.out.println(" 부서 : " + this.deptName);
		//this.getInformation();
		
	}
	
	//(2) public void print() : "수퍼클래스\n서브클래스"란 문구를 출력하는 기능

	@Override
	public void print() {
		super.print();
		System.out.println("서브클래스");
	}

	
	
	
//	public void getInformation(String EmpName, int Salary,String deptName) {
//		this.EmpName = EmpName;
//		this.Salary = Salary;
//		this.deptName = deptName;
//		//System.out.println();
//	}
//	

	
	//생성자
	
	//메소드
	
	
	
}
