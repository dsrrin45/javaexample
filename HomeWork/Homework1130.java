package com.yedam.homework30;

import java.util.Scanner;

public class Homework1130 {

	public static void main(String[] args) {
//		// 1) 문자열 개수 세기
//		-> 입력 문자열에서 알파벳, 숫자, 공백의 개수를 구하시오.
//		예시) 
//		-> 입력 : 1a2b3c4d 5e
//		-> 출력 : 문자 :5개, 숫자:5개, 공백 : 1개
//		
		// 1) 아스키코드
		
		String str = "1a2b3c4d 5e";
		int space = 0;
		int num = 0;
		int cha = 0;
		for(int i=0; i<str.length(); i++) {
			char tempStr = charAt(i); // 한 글자씩 가져오기때문에 문자char형식으로 가져옴
		
			if(tempStr == ' ') {
				space++;
			}else if(tempStr >= '0' && tempStr<'9') { // 아스키 코드 값으로 비교
				
			}else if(tempStr >= 'a' && tempStr<='z') {
				
			}
		}
		
		System.out.println("숫자 : " + num + "개, 문자 : " + cha + "개," + "공백 : " + space);
		
		
		
		
	
		//.length
		//문자 +숫자 +공백  = 문자열 길이
		// 문자열 분리하기 split
		
		Scanner sc = new Scanner(System.in);
//		System.out.println("문자열을 입력 > ");
//		String str = sc.nextLine();
//		System.out.println(str.length());
//		
//		String[] strAry = str.split(",");
//		
//		
//		for(int i=0; i<strAry.length; i++) {
//			System.out.println(i);
//		}
		
//		//2) 중복이 안되는 문자열에서 두 문자사이의 거리 구하기
//		조건 : 입력되는 두 문자를 제외한 가운데 문자의 갯수를 두 문자간 거리로 한다.
//		예시)
//		-> 입력 : "abcdefghijklmnopqrstuvwxyz"
//		-----------------------------------
//		-> 입력 : 첫번째 문자 : c
//		-> 입력 : 두번째 문자 : f
//		-> 출력 : 두 문자간의 거리 : 2
		// 각 문자의 위치를 찾은 다음 큰수 - 작은수
//		------------------------------------
//		-> 입력 : 첫번째 문자 : e
//		-> 입력 : 두번째 문자 : a
//		-> 출력 : 두 문자간의 거리 : 3
//		
		//scanner
		//문자열길이 .length
		//문자열 찾기 indexof //시작하는 위치
		// 문자열 길이 - 해당문자열의 시작하는 위치
		//시작하는위치-시작하는위치?
		System.out.println("입력 : ");
		String str1 = sc.nextLine();
		System.out.println("첫번째 문자 : ");
		String firstChar = sc.nextLine();
		System.out.println("두번째 문자 : ");
		String secondChar = sc.nextLine();
		int index = str1.indexOf(firstChar);
		System.out.println(index);
		int index2 = str1.indexOf(secondChar);
		System.out.println(index2);

		String hh= String.valueOf((index2-index)-1);
		System.out.println("두 문자간의 거리 : "+ hh );
		
		// 각 문자의 위치를 찾은 다음 큰수 - 작은수
		// if(firstIndex < secondIndex) {
//		System.out.println("두문자간의 거리 : " + (secondIndex-firstIndex-1));
//		}else if(firstIndex > secondIndex) {
//			System.out.println("두문자간의 거리 : " + (firstIndex-secondIndex-1));
//		}
		
		//substring(,).length()-1
		
//		
//		//3) 중복문자 제거
//		입력 : aaabbccceedddd
//		출력 : abcd
		
		//중복문자가 존재하는지에 대한 여부
		//str.indexOf(charAt(i)) == i
		///indexOf에서 중복된 문자열이 있으면 -> 중복된 문자열 중에서 제일 첫번째 값
		//반환을 해 줍니다.
		//하나하나의 위치값과 그 위치가 같으면 첫번째값으로 ..a를 찾으면 위치를 첫번째값의 위치를 가져옴......????
		//charAt()
		//인덱스 값(문자 위치)를 입력해서 해당 위치에 있는 문자를 가져 오는것.
		//문자열 찾기
		// 문자열이 몇번째 위치에 존재하는지 확인 = indexOf()
		
		System.out.println("입력 >");
		String str3 = sc.nextLine();
		System.out.println(str3);
		
		String result = ""; // 중복이 되지 않은 문자열을 저장할 문자열 생성
		
		for(int i=0; i<str3.length(); i++) {
			if(str3.indexOf(str3.charAt(i)) == i) {
				
				result += str3.charAt(i);
				
			}
			
		}
		System.out.println(result);
		
		
		
		
		
	}
}
