package com.yedam.homework;

	import java.util.Scanner;

	public class HomeWork002 {
		public static void main(String[] args) {
			// 문제 1
			Scanner scanner = new Scanner(System.in);
		
			System.out.println("x>");
			int x = Integer.parseInt(scanner.nextLine());
			System.out.println("y>");
			int y = Integer.parseInt(scanner.nextLine());
		
			if(x>0 && y>0) {
				System.out.println("제1사분면");
			}else if(x<0 && y>0) {
				System.out.println("제2사분면");
			}else if(x<0 && y<0) {
				System.out.println("제3사분면");
			}else if(x>0 && y<0) {
				System.out.println("제4사분면");
			}
			
			
			// 문제 2
			System.out.println("year>");
			int year = Integer.parseInt(scanner.nextLine());			
			if(year%4==0 && year%100!=0 || year%400==0 ){		
				System.out.println("윤년입니다.");							
			}else {
				System.out.println("윤년이 아닙니다");
			}
			
			// 문제 3
			
			boolean flag = true;
			while (flag) {
			
				int chance = 5;
				while(chance <=5) {
					System.out.println("-----UP&DOWN GAME-----");
					System.out.println(chance +"번의 기회가 있습니다.");
					//chance = chance -1;
					if(chance ==0) {
						break;				
					}
					chance--;
					flag = false;
					int randombox = (int)(Math.random()*50)+1; 
					Scanner sc = new Scanner(System.in);
					System.out.println("답안 입력>");
					int answer = Integer.parseInt(sc.nextLine());
					
					if(randombox!=answer) {
						if(randombox > answer) {
							System.out.println("UP");
						}else {
							System.out.println("DOWN");
						}
							
					}else {
							System.out.println("정답입니다.");
							break;
					}
					
				}
					
					
					
			}
			
			// 문제 4
			Scanner sc2 = new Scanner(System.in);
			System.out.println("---- m * n 문제 ----");
			System.out.println("m>");
			int m = Integer.parseInt(sc2.nextLine());
			System.out.println("n>");
			int n = Integer.parseInt(sc2.nextLine());
			while(m>0) {
				for(int i=1; i<=n; i++) {
					System.out.println(m+"*"+i+"="+ (m*i));
				}
				break;
			}
			
			
			
			
			
		}	
			
			
		}
	


