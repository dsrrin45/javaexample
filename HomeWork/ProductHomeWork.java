package com.yedam.homework;

import java.util.Scanner;

public class ProductHomeWork {
	public static void main(String[] args) {
		// 문제2) 다음은 키보드로부터 상품 수와 상품 가격을 입력받아서 
		///-> Scanner 상품 수, 가격
		/// Scanner sc = new Scanner(System.in); 
		// 최고 가격을 가지는 제품과 해당 제품을 제외한 제품들의 총 합을 구하는 프로그램을 작성하세요.
		///1) 최고 가격 -> max값 찾기, 해당 제품을 제외한 제품들의 총 합 = 모든 제품의 합 - 최고가격
		/// Product pd = null;
		///2-1)최고 가격의 제품을 찾을 때 인덱스 또는 값을 따로 저장 해 놓고
		/// for(int i= 0; i.pd.length; i++) {
		/// 최대값 구하는 로직
		/// 제품의 총합 구하기
		///}
		///2-2) 반복문을 한번 더 돌려서 해당 제품을 제외하고 (조건문) 합을 한다.
		
		// 1) 메뉴는 다음과 같이 구성하세요.
		// 1.상품 수 | 2.상품 및 가격입력 | 3.제품별 가격 | 4.분석 | 5.종료
		/// While(){
		/// 메뉴 출력
		/// 메뉴를 진입할 수 있도록 조건문 통해서 구별을 해 주면 된다.
		/// }
		
		// 2) 입력한 상품 수만큼 상품명과 해당 가격을 입력받을 수 있도록 구현하세요.
		/// 입력한 상품의 수 -> 여러개 담을 수 있는 배열을 써야한다고 도출!
		/// -> 입력한 값에 따라 배열의 사이즈가 바껴야 한다.(고정) -> Scanner 사용해서 배열의 크기를 할당.
		/// 상품 클래스를 만들 때 필드로는 상품명과 가격이 들어가야 한다.
		
		///입력 -> 상품이 배열에 들어가 있으니까 반복문을 활용(배열의 크기만큼 반복)-
		
		// 3) 제품별 가격을 출력하세요.
		//	출력예시, "상품명 : 가격"
		/// 출력 -> 반복문을 활용(배열이 크기만큼 ) 각 방에 있는 객체를 하나씩 꺼내옴.
		/// 객체가 가지고 있는 필드(정보를 담은 변수) -> 출력 예시에 맞게 만든다.
		
		// 4) 분석기능은 최고 가격을 가지는 제품과 해당 제품을 제외한 제품들의 총합을 구합니다.
		
		// 5) 종료 시에는 프로그램을 종료한다고 메세지를 출력하도록 구현하세요.
		/// 반복문을 종료하기 직전에 프로그램 종료한다는 System.out.println() 출력하면 됨.
		
		//if
		
		/// 상품의 수와 상품명을 입력 받을 수 있도록 Scanner 등록
		Scanner sc = new Scanner(System.in);
		Product[] pd = null; // 객체의 선언 변수의 선언들은 while 문 밖에 쓰는게 좋음! // 클래스의 배열의 변수
		int productCount = 0; // 만들고 나서 배열의 크기를 받고 싶어서 밖에서 씀 // 상품의 수
		///Product product = new Product(); ->X 밖에다 쓰면 하나의 번지수 객체에 다가 반복문이 덮어쓰기만 됨
		
		while(true) {
			//if(menu ==1) {
				//Product[] pd = null;	반복문 돌아가면 입력받은 반복후 데이터값 없어짐
				System.out.println(" 1.상품 수 | 2.상품 및 가격입력 | 3.제품별 가격 | 4.분석 | 5.종료");
				System.out.println("입력>");
				
				String selectNo = sc.nextLine();
				// 1번,  상품 수 메뉴 
				if(selectNo.equals("1")) {
					System.out.println("상품 수 입력>");
					productCount = Integer.parseInt(sc.nextLine());
				} else if(selectNo.equals("2")) {
					// 상품 수 입력 받은 내용을 토대로 배열 크기를 확정     	// 배열의 크기만 받는 행동만		// 데이터를 넣을 때 배열의 크기 확정
					pd = new Product[productCount];		// 배열에 다가 객체를 생성하는데 productCount(입력한 상품수) 만큼 받겠다.
					//productCount = 5
					// pd.length = 5
					for(int i=0; i<pd.length; i++) {		
						// 상품 객체 생성 반복문 !
						// 반복문 실행할때 마다 새로운 상품을 만들기 위해서
						// 아래 내용을 정의
						Product product = new Product(); // 100번지, 새로운 상품을 만들기 위해 안에다 넣어줌
						System.out.println((i+1)+"번째 상품");
						System.out.println("상품명>");
						// 1. 변수에 데이터를 입력 받고 객체에 데이트를 넣는 방법
						String name = sc.nextLine();  
						product.ProductName = name;
						
						System.out.println("가격>");
						// 2. 데이터를 입력 받음과 동시에 객체에 데이터를 넣는 방법
						product.price = Integer.parseInt(sc.nextLine()); 
											// 객체에 원하는 정보 다 들어가있는 상태가 됨!
						pd[i] = product;   // 원하는 정보가 들어있는 객체를 배열(각방)에 넣어줌.
						System.out.println("==============================");
					} 
						
					} else if(selectNo.equals("3")) {
						// 배열의 크기만큼 반복문을 진행할때 배열에 각 인덱스(방 번호)를 
						// 활용하여 객채(상품)을 꺼내와서 객체(상품)에 정보를 하나씩 꺼내옴.
						for(int i = 0; i<pd.length; i++) {	//pd[i]는 product객체를 뜻함 
							// String name = product.ProductName; // 같은 말
							String name = pd[i].ProductName;	//.연산자를 통해서 객체의 정보(필드값)를 꺼냄
							int price = pd[i].price;
							
							System.out.println("상품 명 : " + name);
							System.out.println("상품 가격 : " + price);
							System.out.println("상품 가격: " + pd[i].price);
						}
				}else if(selectNo.equals("4")) {
					// 최고 가격을 가지는 제품과 해당 제품을 제외한 제품들의 총 합을 구하는 프로그램을 작성하세요.
					///1) 최고 가격 -> max값 찾기, 해당 제품을 제외한 제품들의 총 합 = 모든 제품의 합 - 최고가격
					/// Product pd = null;
					///2-1)최고 가격의 제품을 찾을 때 인덱스 또는 값을 따로 저장 해 놓고
					/// for(int i= 0; i.pd.length; i++) {
					/// 최대값 구하는 로직
					/// 제품의 총합 구하기
					///}
					///2-2) 반복문을 한번 더 돌려서 해당 제품을 제외하고 (조건문) 합을 한다.
					int max = pd[0].price;
					int sum = 0;
					for(int i =0; i<pd.length; i++) {
						// 제일 비싼 상품 가격 구하기
						if(max < pd[i].price) {
							max = pd[i].price;
						}
						// 상품들의 가격 합계 구하기
						sum  += pd[i].price;
					}
					System.out.println("제일 비싼 상품 가격 : " + max);
					System.out.println("제일 비싼 상품을 제외한 상품 총 합 : " + (sum-max));
				}else if(selectNo.equals("5")) {
					System.out.println("프로그램 종료");
					break; // while문에서 종료시
				}
				
			
			
			//}
			
		}
	}
}
