package com.yedam.stack;
													//5주차 22.12.01 목
import java.util.Stack;
												
///LIFO 컬렉션(후입선출 자료구조; 보관법) - Stack
public class StackExample {

	public static void main(String[] args) {
		Stack<Coin> coinBox = new Stack<>();
		
		coinBox.push(new Coin(100));
		coinBox.push(new Coin(50));
		coinBox.push(new Coin(10));
		coinBox.push(new Coin(500));
		
		while(!coinBox.isEmpty()) {
			Coin coin = coinBox.pop();
			System.out.println("꺼내온 동전 : " + coin.getValue() + "원");
		}
		
		
	}

}
