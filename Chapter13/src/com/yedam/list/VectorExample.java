package com.yedam.list;
											//5주차 22.12.01 목
import java.util.List;
import java.util.Vector;

///List컬렉션 사용하기 2. Vector
public class VectorExample {

	public static void main(String[] args) {
		List<Board> list = new Vector<>(); ///뒤에 <>생략가능 ////list의 board로 생성한 객체만 사용.
		//Board board = new Board("제목1", "내용1", "글쓴이1");
		///-> 아래내용과 같은 내용. 변수에 담을 필요없이 new 연산자로 메모리 활당..
		/// 객체를 생성해서 첫번째방에 넣는다.
		list.add(new Board("제목1", "내용1", "글쓴이1"));
		list.add(new Board("제목2", "내용2", "글쓴이2"));
		list.add(new Board("제목3", "내용3", "글쓴이3"));
		list.add(new Board("제목4", "내용4", "글쓴이4"));
		list.add(new Board("제목5", "내용5", "글쓴이5"));
		/// 정보가 담긴 객체가 각 방에 들어가게 됨.
		
	
		for(int i = 0; i<list.size(); i++) {
			//첫번째 방법
			/// 객체를 변수에 담아서 변수를 통해서 가져오는 방법
			Board board = list.get(i);
			System.out.println(board.subject + "\t" + board.content + "\t" + board.writer);
		
			//두번째 방법
			///각 방 객체에 있는 필드를 바로 꺼내옴 /// list.get(i)는 객체.
			System.out.println(list.get(i).subject + "\t" + list.get(i).content + "\t" + list.get(i).writer);
		}
	
		list.remove(2);
		System.out.println();
		
		for(int i = 0; i<list.size(); i++) {
			//첫번째 방법
			/// 객체를 변수에 담아서 변수를 통해서 가져오는 방법
			Board board = list.get(i);
			System.out.println(board.subject + "\t" + board.content + "\t" + board.writer);
		
//			//두번째 방법
//			///각 방 객체에 있는 필드를 바로 꺼내옴 /// list.get(i)는 객체.
//			System.out.println(list.get(i).subject + "\t" + list.get(i).content + "\t" + list.get(i).writer);
		}
		
		
		
		
	
	
	
	}

}
