package com.yedam.list;
												//5주차 22.12.01 목
import java.util.ArrayList;
import java.util.List;


///인덱스를 통해 관리하는데 안에 객체가 들어가야함 
///<>:제너릭타입를 통해서 무슨 객체인지 알려줄 수 있음.
///List컬렉션 사용하기 1. ArrayList
public class ListExample {

	public static void main(String[] args) {
		//List<E>
		List<String> list = new ArrayList<String>();
	
		list.add("java"); // 인덱스가 0인 곳에 추가
		list.add("JDBC"); // 인덱스가 1인 곳에 추가
		list.add("Servlet/JSP"); // 인덱스가 2인 곳에 추가
		// 인덱스가 2인 곳에 추가를 해주세요
		//-> JSP : 인덱스 3으로 밀리고 DataBase가 인덱스 2에 추가
		list.add(2,"DataBase"); // 0 -> 자바, 1 -> JDBC, 2 -> DataBase, 3 -> JSP
		list.add("iBATIS"); // 인덱스가 4인 곳에 추가
		
		
		//list 크기, 배열의 length == list의 size
		int size = list.size();
		System.out.println("총 객체 수 : " + size);
		System.out.println();
		
		//list 객체 가져오기	.get() 사용.
		String skill = list.get(2); ///index가 2인 데이터를 가져오라.
		System.out.println("Index 2 : " + skill);
		
		// list 크기만큼 반복문 돌리는 방법
		for(int i = 0; i<list.size(); i++) {
			String str = list.get(i);	/// i(=인덱스;자리)를 이용하여 배열의 데이터를 가져옴
			System.out.println(i + " : " + str);
		}
		System.out.println();
		
		
		list.remove(2);
		
		for(int i = 0; i<list.size(); i++) {
			String str = list.get(i);	/// i(=인덱스;자리)를 이용하여 배열의 데이터를 가져옴
			System.out.println(i + " : " + str);
		}
		
		/// -> 데이터가 지워지고 당겨짐을 확인 가능.
		
		System.out.println();

		list.remove("JDBC"); ///  들어있는 객체를 활용해서 지울 수도 있다.
		
		for(int i = 0; i<list.size(); i++) {
			String str = list.get(i);	/// i(=인덱스;자리)를 이용하여 배열의 데이터를 가져옴
			System.out.println(i + " : " + str);
		}
		
		
		
	}

}
