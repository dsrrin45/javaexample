package com.yedam.list;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
															//5주차 22.12.01 목
import javax.print.DocFlavor.STRING;

///List컬렉션 사용하기 3. LinkedList
// List간 실행 성능 비교
public class SpeedCheck {

	public static void main(String[] args) {
		List<String> aList = new ArrayList<>();
		List<String> lList = new LinkedList<>();
		
		long startTime;
		long endTime;
		
		startTime = System.nanoTime();
		for(int i = 0; i<100000; i++) {
			aList.add(0, String.valueOf(i));
		}
		endTime = System.nanoTime();
		
		System.out.println("ArrayList 걸린 시간 : " + (endTime-startTime)+"ns");
		
		startTime = System.nanoTime()	;
		for(int i=0; i<100000; i++) {
			lList.add(0, String.valueOf(i));
		}
		endTime = System.nanoTime()	;
		
		System.out.println("LinkedList 걸린 시간 : " + (endTime-startTime)+"ns");
	}

}
