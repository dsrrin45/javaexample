package com.yedam.map;
															//5주차 22.12.01 목
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

///Map컬렉션 사용하기 1. HashMap
public class HashMapExample {

	public static void main(String[] args) {
		//객체의 저장
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		map.put("고길동", 85);
		map.put("김또치", 90);
		map.put("고희동", 80);
		map.put("김또치", 33);
		System.out.println("총 Entry 수 : " + map.size());	///중복값은 덮어쓰기 됨.
		
		//객체 찾기
		System.out.println(map.get("김또치"));
		System.out.println();
		
		//객체를 하나씩 처리
		Set<String> keySet = map.keySet(); /// 열쇠를 다가져오기
		Iterator<String> keyIterator = keySet.iterator();
		while(keyIterator.hasNext()) {
			String key = keyIterator.next();
			Integer value = map.get(key);
			System.out.println("key : " + key + " value : " + value);
		}
		
		//객체 삭제
		map.remove("고길동");
		System.out.println("총 Entry 수 : " + map.size());
		
		//객체를 하나씩 ㅣ처리
		/// Map에는 <Map.Entry<String, Integer>> 형태로 저장되어 있다.
		Set<Map.Entry<String, Integer>> entrySet = map.entrySet();
		///반복자를 통해 형태에 맞게 데이터 출력 ///entryIterator에 형태 저장
		Iterator<Map.Entry<String, Integer>> entryIterator = entrySet.iterator(); 
		
		
		while(entryIterator.hasNext()) {
			Map.Entry<String, Integer> entry = entryIterator.next();
			String key = entry.getKey();
			Integer value = entry.getValue();
			System.out.println("key : " + key + " value : " + value);
		}
		System.out.println();
		
		//객체 전체 삭제
		map.clear();
		System.out.println("총 Entry 수 : " + map.size());
		
		
	
		
		
		
		
		
		
		
	}

}
