package com.yedam.set;
														//5주차 22.12.01 목
import java.util.HashSet;
import java.util.Set;

///Set컬렉션 사용하기 1. HashSet
public class HashSetExample2 {

	public static void main(String[] args) {
		Set<Member> set = new HashSet<Member>();
		
		///new 연산자를 사용해서 객체를 넣었기 때문에 서로 다른 객체!
//		set.add(new Member("고길동", 30)); // 고길동의 해시코드 : 1
//		set.add(new Member("고길동", 30)); // 고길동의 해시코드 : 1
		
		set.add(new Member("고길동", 30)); // 고길동의 해시코드 : 1 + 30 -> 하나의 정수를 만듬 비교하려는 대상이랑 1번 째로 해시코드값이 같은가? 2번 째로 equals 해서 비교하려는 두 객체가 같다 하면 같은객체라고 인식
		set.add(new Member("고길동", 25)); // 고길동의 해시코드 : 1 + 25
		///-> 하나의 정수를 만듬 비교하려는 대상이랑 1번 째로 해시코드값이 같은가?
		///2번 째로 equals 해서 비교하려는 두 객체가 같다 하면 같은객체라고 인식
		
		System.out.println("총 객체수 : " + set.size());
	
	///해시코드를 재정의해서 동등객체로 만들기
	///비교 해시코드와 equals를 재정의해서 같은 객체인지 확인.
	
	
	}

}
