package com.yedam.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

//5주차 22.12.01 목

///같은 객체인지 비교 객체마자 존재하는 해시코드 값으로(고유 주소번지를 숫자로 바꾸는 것?) 
///set 타입 : 동일한 객체 넣지 못함 -> 같으면 넣지 못하고 , 다르면 저장
///Set컬렉션 사용하기 1. HashSet
public class HashSetExample {
	public static void main(String[] args) {
		Set<String> set = new HashSet<>();
		//String str = "Java"
		//String str2 = "Java"
		///new를 사용하지 않고 String값을 넣어주면 데이터 메모리영역을 같이 공유 => 같은 객체로 인식.
		
		set.add("Java");
		set.add("JDBC");
		set.add("Servlet/JSP");
		set.add("Java");
		set.add("iBatis");
	
		int size = set.size();
		System.out.println("총 객체 수 : " + size);
		
		Iterator<String> iterator = set.iterator(); ///set이 데이터를 가져올 수 있도록 반복자 이용.
		while(iterator.hasNext()) {	///hasNext() 존재한다면(읽어올 요소가 남아있는지 확인)
			String element = iterator.next();
			System.out.println("\t" + element);
		}
		
		set.remove("Java");
		set.remove("JDBC");
		///-> 객체는 데이터가 존재하지 않기때문에 데이터를 찾아서 지워줌(..?)
		
		//향상된 for문
		for(String temp : set) {	///set에 담긴 물건이 존재할 때까지 반복 진행. //(데이터를 꺼내고 temp에 담아주고 다시 꺼내는 작업 반복)
									/// -> (데이터를 꺼내고 temp에 담아주고 다시 꺼내는 작업 반복)
			System.out.println("\t" + temp);
		
		}
		
		set.clear();
		
		System.out.println("총 객체 수 : " + set.size());
		
		if(set.isEmpty()) {
			System.out.println("객체가 존재하지 않습니다.");
		}
		
		
		
		
		
	}
	
	

}
