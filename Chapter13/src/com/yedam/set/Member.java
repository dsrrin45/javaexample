package com.yedam.set;
													//5주차 22.12.01 목
///Set컬렉션 사용하기 1. HashSet
public class Member {
	//필드
	public String name;
	public int age;
	
	//생성자
	public Member(String name, int age) {
		this.name = name;
		this.age = age;
	}

	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Member) {
			Member member = (Member) obj;
			return member.name.equals(this.name) && (member.age==age);
			///member.name은 외부에서 가져온것 ///this.name은 내부에서 가져온것.
		}
		return false;

	}
	///object는 모든 클래스의 부모격

	///객체가 가진 메모리 코드를 숫자로 바꿈
	@Override
	public int hashCode() {
		return name.hashCode();// + age;(다르면)

	}

	






}
