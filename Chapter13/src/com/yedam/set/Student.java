package com.yedam.set;
													//5주차 22.12.01 목			

///Map컬렉션 사용하기 1. HashMap
public class Student {
	public int sno;
	public String name;
	
	public Student(int sno, String name) {
		this.sno = sno;
		this.name = name;
	}

	/// 같은 객체로 인식하도록 하기 위해 재정의
	@Override
	public int hashCode() {
		return sno + name.hashCode();
	}

	@Override
	public boolean equals(Object obj) { 
		if(obj instanceof Student) { /// 매개변수로 들어온 애가 스튜던트 클래스로 만들어진 애인가?
			Student std = (Student) obj;	/// 그러면 강제 타입변환
			return (sno==std.sno) && (name.equals(std.name));		
		}
		return false;
	}
	

	
	
	
	
	
	
	
	
	
}
