package com.yedam.set;
														//5주차 22.12.01 목
import java.util.HashMap;
import java.util.Map;

///Map컬렉션 사용하기 1. HashMap
public class HashMapExample2 {

	public static void main(String[] args) {
		Map<Student, Integer> map = new HashMap<Student, Integer>();
		
		map.put(new Student(1,"홍길동"), 95);
		map.put(new Student(1,"홍길동"), 95);
		
		System.out.println("총 Entry 수 : " + map.size());

	}
}
