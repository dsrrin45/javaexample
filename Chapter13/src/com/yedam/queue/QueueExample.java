package com.yedam.queue;
												//5주차 22.12.01 목

import java.util.LinkedList;
import java.util.Queue;

///FIFO 컬렉션(선입선출 자료구조; 보관법) - Queue
public class QueueExample {

	public static void main(String[] args) {
		Queue<Message> messageQueue = new LinkedList<Message>();
		
		messageQueue.offer(new Message("sendMail", "홍길동"));
		messageQueue.offer(new Message("sendSMS","김또치"));
		messageQueue.offer(new Message("sendKaKaoTalk", "마이콜"));
		
		while(!messageQueue.isEmpty()) {
			///큐를 메세지 타입으로 생성되었으니까 메세지 타입으로 객체를 생성해야함.
			Message message = messageQueue.poll();
		
			switch (message.command) {
			case "sendMail":
				System.out.println(message.to + "님에게 메일을 보냅니다.");
				break;
			case "sendSMS":
				System.out.println(message.to + "님에게 문자를 보냅니다.");
				break;
			case "sendKaKaoTalk":
				System.out.println(message.to + "님에게 카톡을 보냅니다.");
				break;
			default:
				break;
			}
		}
	}

}
