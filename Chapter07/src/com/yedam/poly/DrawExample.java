package com.yedam.poly;
									// 5주차 22.11.28 월
public class DrawExample {

	public static void main(String[] args) {
		//자동타입변환
		//부모 클래스 변수 = new 자식클래스();
		
		// 다형성을 구현
		
		// 부모클래스 Draw를 자식circle로 초기화
		Draw figure = new Circle(); // Draw가 가지고 있는것만 쓸수있음
		
		figure.x = 1;
		figure.y = 2;
		figure.draw(); // 부모클래스에 정의된것 중 자식클래스가 재정의한것을 가져옴.
		
		figure = new Rectangle();	//Rectangle로 클래스를 초기화
		
		figure.draw();	// Rectangle의 draw() 실행
		
		// 다형성 -> 부모클래스 하나로 여러 명의 자식을 넣었다가 뺏다가 맞추는거
		
	}

}
