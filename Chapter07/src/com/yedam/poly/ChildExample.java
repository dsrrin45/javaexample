package com.yedam.poly;

public class ChildExample {
	public static void main(String[] args) {
//		Child child = new Child();
		
		// ☆★클래스간의 자동타입변환★☆
		// 부모 클래스에 있는 메소드를 사용하되
		// 단, 자식 클래스에 재정의가 되어 있으면 
		// 자식 클래스에 재정의된 메소드를 사용 하겠습니다. (부모에 없으면 실행 안됨)
		
//		Parent parent = child;	// 부모가 가지고 있는 것을 parent가 실행하게끔함.
//		
//		parent.method1();
//		parent.method2();
//		//parent.method3();	// 부모클래스에 없기때문에 안됨.
//		
//		
//		
		// 클래스간의 강제 타입 변환
		// 자동타입변환으로 인해서 자식클래스 내부에 정의된 필드, 메소드를 못 쓸 경우
		// 강제타입변환을 함으로써 자식 클래스 내부에 정의된 필드와 메소드를 사용.
		Parent parent = new Child();
		
		parent.field = "data1";
		parent.method1();
		parent.method2();
//		parent.field2 = "data2";	// 불가능
//		parent.method3();
		
		Child child = (Child) parent;	// 강제 타입변환
		child.field2 = "data2";
		child.field = "data";
		child.method3();
		child.method1();
		child.method2();
		
		
		// 클래스 타입 확인 예제
		
		method1(new Parent());
		method1(new Child());
		
		GrandParent gp = new Child();	// 자동 타입변환 가능(g<-p<-c) // 할아버지는 손자의 내용을 가지고 출력가능
		gp.method4();
	}
	//자동타입변환된 애만 강제 캐스팅 가능.
	
	public static void method1(Parent parent) {	// 매개 변수를 통한 다양성 구현(perent타입 은 자기자신으로 만든거, new로 만든건 child로 만든거)
		if(parent instanceof Child) {
			Child child = (Child) parent;
			System.out.println("변환 성공");
		} else {
			System.out.println("변환 실패");
		}
	}
	
}
