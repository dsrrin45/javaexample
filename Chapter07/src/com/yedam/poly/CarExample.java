package com.yedam.poly;

public class CarExample {

	public static void main(String[] args) {
		Car car = new Car();
		
		for(int i = 0; i<=5; i++) {	// 다섯번 돌 것이다.
			int problemLoc = car.run(); // 문제가 생긴 차의 번호저장.
		
			switch(problemLoc) {
			//frontLeftTire
			case 1:
				System.out.println("앞 왼쪽 HankookTire 교환");
				//Tire = 부모클래스(슈퍼클래스)
				//HankookTire = 자식클래스(서브클래스)
				//frontLeftTire = new HanKookTire("앞왼쪽", 15);
				car.frontLeftTire = new HanKookTire("앞왼쪽", 15);	// 베이스는 부모걸 쓰지만 오버라이딩은 자식껄로 쓰겠습니다.
				break;
			case 2:
				System.out.println("앞 오른쪽 KumhoTire 교환");
				car.frontRightTire = new KumhoTire("앞오른쪽", 15);
				break;
			case 3:
				System.out.println("뒤 왼쪽 HankookTire 교환");
				car.backLeftTire = new HanKookTire("왼뒤쪽", 15)
				break;
			case 4:
				System.out.println("뒤 오른쪽 KumhoTire 교환");
				car.backRightTire = new KumhoTire("왼오른쪽", 15);
				break;
			}
			System.out.println("============================");
		
		}
	}

}
