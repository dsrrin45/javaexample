package com.yedam.inheri;

public class Person extends People {

	public int age;
	// 자식 객체를 만들때, 생성자를 통해서 만든다.
	// super()를 통해서 부모 객체를 생성한다.
	// 여기서 super() 의미하는 것은 부모의 생성자를 호출
	// 따라서 자식 객체를 만들게 되면 부모 객체도 같이 만들어진다.
	
	public Person(String name, String ssn) {
		super(name, ssn); // 부모에 있는 필드나, 메소드를 가져올때 super사용
		 // 자식 객체를 만들때(=생성자를 사용할때) 부모 객체를 만든다는 뜻(suepr 생성자).
		
		
		
		
		
		
	}
}
