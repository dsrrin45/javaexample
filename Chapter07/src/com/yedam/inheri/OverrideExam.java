package com.yedam.inheri;

public class OverrideExam {
	public static void main(String[] args) {
		Child child = new Child();
		
		// Overrading 덮어쓰기
		child.method1();
		
		child.method2();
		
		child.method3();
	}
}
