package com.yedam.inheri;

public class DmbPhoneExample { // 실행하려면 main 문 사용
	public static void main(String[] args) {
		DmbCellPhone dmb = new DmbCellPhone("자바폰", "검정", 10);
		
		// 부모 클래스의 필드 호출
		System.out.println(dmb.model);
		System.out.println(dmb.color);
		
		// 자식 클래스의 필드 호출
		System.out.println(dmb.channel);
		
		//부모 클래스의 메소드 호출
		dmb.powerOn();
		dmb.bell();
		dmb.hangUp();
		
		// 자식 클래스의 메소드 호출
		dmb.turnOnDmb();
		dmb.turnOffDmb();
		
		// 부모 클래스의 메소드 호출
		dmb.powerOff();
		
		
	}
}
