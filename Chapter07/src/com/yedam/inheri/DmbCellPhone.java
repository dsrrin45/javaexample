package com.yedam.inheri;

public class DmbCellPhone extends CellPhone {
	//필드
	int channel;
	
	// 생성자 // 기본값, 초기화
	public DmbCellPhone(String model, String color, int channel) {
		this.model = model;
		this.color = color;
		this.channel = channel;
		
	}
	
	// 메소드
	
	void turnOnDmb() {
		System.out.println("채널 " + channel + "번 방송 수신");
	}
	
	void turnOffDmb() {
	}
	
	
}
