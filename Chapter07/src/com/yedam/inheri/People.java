package com.yedam.inheri;

public class People {
	// 필드
	public String name;
	public String ssn;
	
	// 생성자
	public People(String name, String ssn) {	// 생성자를 통해 필드를 초기화
		this.name = name;
		this.ssn = ssn;
	}
	
	// 메소드
}
