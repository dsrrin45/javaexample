package com.yedam.inheri;

public class Child extends Parent {
	public String lastName;	// 부모로 부터 상속받지 않을 것들을 정의
	public int age;
	
	// 메소드
	// 오버라이딩 예제
	
	@Override
	public void method1() {	// 부모접근제한자보다 강하면 안됨.
		System.out.println("child class -> method1 Override");
	}
	
	public void method3() {
		System.out.println("child class -> method3 Call");
	}

	@Override
	public void method2() {
		// TODO Auto-generated method stub
		super.method2();
	}
	// 우클릭 > source > Overraide 자동 완성
	

}
