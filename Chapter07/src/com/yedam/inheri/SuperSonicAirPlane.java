package com.yedam.inheri;

public class SuperSonicAirPlane extends AirPlane {
	// 필드
	// 일반비행
	public static final int NORMAl = 1;
	// 초음속 비행
	public static final int SUPERSONIC = 2;
	
	public int flyMode = NORMAl;
	// 생성자
	
	
	//메소드
	
	@Override
	public void fly() {
		if(flyMode == SUPERSONIC) {
			System.out.println("초음속 비행 모드");
		} else {
			super.fly();		// super. 부모가 가지고 있는 메소드를 가지고 올때
		}
	}
	
	
	
	
}
