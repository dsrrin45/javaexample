package com.yedam.inheri;

public class AirPlaneExample {

	public static void main(String[] args) {
		SuperSonicAirPlane sa = new SuperSonicAirPlane();
		
		sa.takeoff();
		
		sa.fly();
		
		sa.flyMode = SuperSonicAirPlane.SUPERSONIC; 	// Static 필드는 바로 가져와서 사용 가능
														// 기본값 변경
		sa.fly();
		
		sa.flyMode = SuperSonicAirPlane.NORMAl;
		
		sa.fly();
		
		sa.land();
	}

}
