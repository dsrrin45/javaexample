package com.yedam.abs;

public class AnimalExample {
	public static void main(String[] args) {
		
		Cat cat = new Cat();
		cat.sound();
		
		System.out.println();
		
		Animal animal = new Cat(); // 추상클래스를 자식 클래스를 활용해서 자동타입변환.
		animal.sound();
		animal.breath();
		System.out.println(animal.kind);
	
		animalSound(new Cat());
		
		// 추상클래스 -> 클래스와 별반 다른게 없다.
		// 차이점 
		// 1) 자기 자신을 객체로 만들지 못한다.
		// 2) 추상 메소드가 존재하며, 상속을 받게 되면 추상메소드는 반드시 구현해야한다.
		// 3) 스스로 객체(인스턴스)화 안되므로 자식클래스를 통한 자동타입변환으로 구현.
	}
	
	// 매개 변수를 활용한 자동타입변환
	public static void animalSound(Animal animal) {
		animal.sound();
	}
}
