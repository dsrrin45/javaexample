package com.yedam.abs;

public class PhoneExample {
	public static void main(String[] args) {
		// 추상 클래스 객체(인스턴스)화 확인
		//Phone phone = new Phone("Owner"); // 추상클래스는 자기자신 객체 못만듬
		SmartPhone smartPhone = new SmartPhone("홍길동");
		
		smartPhone.turnOn();
		smartPhone.turnOff();
		
		smartPhone.internetSearch();
		
	}
	
}
