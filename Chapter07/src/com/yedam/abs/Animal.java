package com.yedam.abs;

public abstract class Animal {

	public String kind;
	
	public void breath() {
		System.out.println("숨을 쉽니다");
	}
	// 추상메소드
	// 상속을 받게 되면 무조건 ! 오버라이딩 해야하는 메소드 = 추상메소드
	public abstract void sound();
}
