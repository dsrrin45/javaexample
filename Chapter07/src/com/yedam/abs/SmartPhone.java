package com.yedam.abs;

public class SmartPhone extends Phone {
	//
	public SmartPhone(String owner) {
		super(owner);
	}
	
	//메소드
	public void internetSearch() {
		System.out.println("인터넷을 검색합니다.");
	}
}
